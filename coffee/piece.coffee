root = if global? then global else window

class Piece
  constructor: (args) ->
    @id = "p_" + root.Utils.create_id()
    @type = args.type
    @owner_id = args.owner.id
    @direction = args.direction
    @is_promoted = false
    @is_living = true
    if root.DB.id_piece_map[@id]
      throw "already this piece is created: #{@id}"
    root.DB.id_piece_map[@id] = @
  die: ->
    @is_living = false
    @direction = 'u'
    @is_promoted = false
  remove_from_the_game: ->
    delete root.DB.id_piece_map[@id]
  promote_if_can: (prev_grid, next_grid) ->
    if (prev_grid.rular_id && prev_grid.rular_id != @owner_id) || (next_grid.rular_id && next_grid.rular_id != @owner_id)
      @is_promoted = true
  revive: ->
    @is_living = true
  owner: ->
    root.DB.id_user_map[@owner_id]
  name: ->
    @type
  toString: ->
    @owner_id + @type + @direction
  set_direction: (direction) ->
    if Piece.is_valid_direction(direction)
      if @is_living
        if @owner().is_available_time()
          @direction = direction
          @owner().last_put_time = new Date
          true
        else
          false
      else
        @direction = direction
        true
    else
      console.log 'invalid direction'
      false
  @is_valid_direction = (direction) ->
    Object.keys(@DIRECTIONS).some(direction)
  @DIRECTIONS = {
    u: '上'
    l: '左'
    d: '下'
    r: '右'
  }
  @get_by_id = (id) ->
    root.DB.id_piece_map[id]

root.Piece = Piece
