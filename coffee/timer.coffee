root = if global? then global else window

class Timer
  constructor: ->
    @times = []
  start: ->
    if root.libs?.microtime
      @first = root.libs.microtime.now()
    else
      @first = (new Date).getTime()
    @lap 'start'
  @create_and_start = ->
    t = new Timer
    t.start()
    t
  lap: (msg) ->
    if root.libs?.microtime
      @times.push([msg, root.libs.microtime.now()])
    else
      @times.push([msg, (new Date).getTime()])
  end: ->
    @lap 'end'
  show: (use_console_log = true) ->
    #first = @times.unshift()
    #console.log first
    #console.log @first
    @lap 'timer#show called'
    if @times.last().last() == @first
      report = "(no time)"
    else
      report = @times.map((msg_and_time) =>
        str = "#{msg_and_time[1] - @first}"
        if msg_and_time[0]
          if root.libs?.microtime
            str += " microsec at #{msg_and_time[0]}"
          else
            str += " msec at #{msg_and_time[0]}"
        str
      ).join("\n")
      if use_console_log
        console.log report
    report

module.exports = Timer
#timer = new Timer
#timer.start()
#timer.lap()
#setTimeout ->
#  timer.lap()
#  timer.end()
#  console.log timer.show()
#, 1000

