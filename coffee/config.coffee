root = if global? then global else window
root.Config = 
  port: 7777
  host: 'localhost'
  waiting_second: 2
