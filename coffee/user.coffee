root = if global? then global else window

class User
  constructor: (space) ->
    @id = "u_" + root.Utils.create_id()
    @color = "rgb(#{Number.random(0,255)}, #{Number.random(0,255)}, #{Number.random(0,255)})"
    @is_connected = undefined
    @having_piece_ids = []
    @is_joining = false
    space.users.push(@)
    root.DB.id_user_map[@id] = @
    @last_put_time = Date.create(2000, 1, 1)
    @_name = null
  set_name: (name) -> @_name = name
  name: ->
    (@_name || @id ) + "さん"
  name_html_tag: ->
    "<span class='user_name' data-user_id='#{@id}'>#{@name()}</span>"
  on_connection_closed: ->
    @is_connected = false
  on_connection_opened: ->
    @is_connected = true
  gain_piece: (piece, space) ->
    piece.die()
    if piece.type == '王'
      @slay(piece.owner(), space)
      piece.remove_from_the_game()
    else
      piece.owner_id = @id
      @having_piece_ids.push piece.id
  slay: (opponent, space) ->
    IS_PIECE_REMAINING = true
    User.get_all_pieces(opponent.id).forEach (piece) =>
      if IS_PIECE_REMAINING
        piece.owner_id = @id
      else
        @gain_piece(piece, space)
    space.get_territory_grids_of(opponent).forEach (grid) =>
      grid.rular_id = @id
    @having_piece_ids.include opponent.having_piece_ids
    opponent.having_piece_ids = []
    opponent.is_joining = false
  @get_all_pieces = (user_id) ->
    Object.values(root.DB.id_piece_map).filter (piece) =>
      piece.owner_id == user_id
  having_pieces: () ->
    @having_piece_ids.map (piece_id) ->
      root.DB.id_piece_map[piece_id]
  having_grids: () ->
    @having_piece_ids.map (piece_id) ->
      root.DB.id_piece_map[piece_id]
  is_available_time: ->
    ((new Date).getTime() - @last_put_time.getTime()) / 1000 > root.Config.waiting_second

root.User = User
