l = console.log
fs = require 'fs'
path = require "path"
app_root = "/Users/hedachi/InfinityJs"

Server = require "#{app_root}/js/server"
Grid = require "#{app_root}/js/grid"
Space = require "#{app_root}/js/utils"
Utils = require "#{app_root}/js/space"

describe 'Server', ->
  it 'can instantiate', ->
    server = new Server
    expect(server.say()).toBe('hello')
    
