root = if global? then global else window

l = console.log

if global?
  fs = require 'fs'
  path = require "path"
  app_root = "/Users/hedachi/InfinityJs"
  require "#{app_root}/js/grid"
  require "#{app_root}/js/space"
  require "#{app_root}/js/server"
  require "#{app_root}/js/utils"
  require "#{app_root}/js/piece"
  require "#{app_root}/js/config"
  require "#{app_root}/js/user"
  require "#{app_root}/js/piece_movement"

User = root.User
Space = root.Space
Piece = root.Piece
PieceMovement = root.PieceMovement
Grid = root.Grid
Utils = root.Utils
Config = root.Config

root.DB =
  id_piece_map : {}
  id_user_map : {}

describe 'Space', ->
  beforeEach ->
    root.DB =
      id_piece_map : {}
      id_user_map : {}

  it 'can create grid and count correctly', ->
    space = new Space([5,5])
    expect(space.count_grid()).toBe(36)
    space.expand([8,8])
    expect(space.count_grid()).toBe(81)

  it 'can calculate x and y lengths', ->
    space = new Space([5,3])
    expect(space.x_length()).toBe(6)
    expect(space.y_length()).toBe(4)

  it 'can show console', ->
    space = new Space([4,6])
    user1 = new User(space)
    user2 = new User(space)

    space.get_grid([2,1]).put new Piece
      type: '歩'
      owner: user1
      direction: 'd'
    space.get_grid([2,2]).put new Piece
      type: '歩'
      owner: user2
      direction: 'u'
    space.get_grid([2,3]).put new Piece
      type: '飛'
      owner: user2
      direction: 'r'
    space.create_piece [4,4], '金', user2, 'd'

    expect(space.count_grid()).toBe(35)
    #console.log space.to_json()

  it 'create_an_army', ->
    space = new Space([10,10])
    user1 = new User(space)
    space.create_an_army(user1, 'd', [1,1])
    user2 = new User(space)
    space.create_an_army(user2, 'u', [1,7])

  #it 'can find piece by piece_id', ->
  #  space = new Space([10,10])
  #  space.create_an_army(new User(space), 'd', [1,1])
  #  id = space._final_piece.id
  #  expect(space.find_piece_by_id(id).id).toBe(id)

  it 'can move piece', ->
    space = new Space([10, 10])
    piece = space.create_piece [4, 4], '金', new User(space), 'd'
    expect(space.get_grid([4, 4]).piece.type).toBe('金')
    expect(space.get_grid([4, 5]).piece).toBe(null)

    space.put_piece piece.id, 4, 5
    expect(space.get_grid([4, 4]).piece).toBe(null)
    expect(space.get_grid([4, 5]).piece.type).toBe('金')

    space.put_piece piece.id, 4, 4 # user is not ready to put piece because of time restriction
    expect(space.get_grid([4, 4]).piece).toBe(null)
    expect(space.get_grid([4, 5]).piece.type).toBe('金')

    spyOn(piece.owner(), 'is_available_time').andReturn(true);

    space.put_piece piece.id, 4, 4
    expect(space.get_grid([4, 4]).piece.type).toBe('金')
    expect(space.get_grid([4, 5]).piece).toBe(null)

  it "can't move to wall", ->
    space = new Space([10, 10])
    piece = space.create_piece [4, 4], '金', new User(space), 'd'

    space.get_grid([4, 5]).is_wall = true
    space.put_piece piece.id, 4, 5
    expect(space.get_grid([4, 5]).piece).toBe(null)
    expect(space.get_grid([4, 4]).piece.type).toBe('金')

    space.get_grid([4, 5]).is_wall = false
    space.put_piece piece.id, 4, 5
    expect(space.get_grid([4, 4]).piece).toBe(null)
    expect(space.get_grid([4, 5]).piece.type).toBe('金')

  it "can't move to not movable grid", ->
    space = new Space([20, 20])
    user = new User(space)
    spyOn(user, 'is_available_time').andReturn(true)

    piece = space.create_piece [10, 6], '桂', user, 'd'
    space.put_piece piece.id, 4, 5
    expect(space.get_grid([10, 6]).piece?.type).toBe('桂')

    space.put_piece piece.id, 11, 8
    expect(space.get_grid([11, 8]).piece?.type).toBe('桂')

    piece.set_direction 'u'
    space.put_piece piece.id, 9, 8
    expect(space.get_grid([11, 8]).piece?.type).toBe('桂')

    space.put_piece piece.id, 10, 6
    expect(space.get_grid([10, 6]).piece?.type).toBe('桂')

    piece.set_direction 'd'
    space.put_piece piece.id, 9, 8
    expect(space.get_grid([9, 8]).piece?.type).toBe('桂')


  it 'can search piece', ->
    space = new Space([5, 5])
    piece = space.create_piece [3, 1], '金', new User(space), 'd'
    found_pieces = space.search_piece([-2, -1], [7, 2])
    expect(found_pieces.length).toBe(1)
    expect(found_pieces.first()).toBe(piece)

  it 'can create armies in nice place', ->
    space = new Space([10, 10])
    space.create_an_army(new User(space), 'd', [0, 0])

  it 'find empty area', ->
    space = new Space([0, 0])
    space.get_empty_area()
    #found_pieces = space.search_piece([0,0], [0,0])
    
  it 'can get start area map', ->
    space = new Space([0, 0])
    coordinate_3 = space.start_area_coordinate_map()[3]
    expect(coordinate_3[0]).toBe(33)
    expect(coordinate_3[1]).toBe(43)

  it 'can found empty area', ->
    space = new Space([20, 10])
    user = new User(space)
    expect(space.get_empty_area().from).toEqual([0,0])
    expect(space.get_empty_area().to).toEqual([10,10])
    space.create_piece [3, 1], '金', user, 'd'
    expect(space.get_empty_area().from).toEqual([11,0])
    expect(space.get_empty_area().to).toEqual([21,10])
    space.create_piece [11, 1], '金', user, 'd'
    expect(space.get_empty_area().from).toEqual([11,11])
    expect(space.get_empty_area().to).toEqual([21,21])

  it 'can attack to enemy piece', ->
    space = new Space([10, 10])
    user1 = new User(space)
    user2 = new User(space)
    piece1 = space.create_piece [4, 4], '金', user1, 'd'
    piece2 = space.create_piece [4, 5], '金', user2, 'd'
    space.put_piece piece1.id, 4, 5

  it 'can get all grids as array', ->
    space = new Space([11, 11])
    expect(space.get_all_grids_as_array().length).toBe(144)

  it 'can get terrytory grids', ->
    space = new Space([11, 11])
    user1 = new User(space)
    user2 = new User(space)
    space.create_an_army(user1, 'd', [10, 10])
    expect(space.get_territory_grids_of(user1).length).toBe(44)
    expect(space.get_territory_grids_of(user2).length).toBe(0)
    space.create_an_army(user2, 'd', [10, 10])
    expect(space.get_territory_grids_of(user2).length).toBe(44)

  it 'jan get_empty_area_for_horizontally_long_mode', ->
    space = new Space([0, 0])
    user1 = new User(space)

    from_and_direction = space.get_empty_area_for_horizontally_long_mode()
    expect(from_and_direction).toEqual
      from: [0,0]
      direction: 'd'
    space.create_an_army user1, from_and_direction.direction, from_and_direction.from, 'horizontally'
    expect(space.get_grid([0, 0]).piece?.type).toBe('香')

    from_and_direction = space.get_empty_area_for_horizontally_long_mode()
    expect(from_and_direction).toEqual
      from: [5,6]
      direction: 'u'
    space.create_an_army user1, from_and_direction.direction, from_and_direction.from, 'horizontally'
    expect(space.get_grid([5, 8]).piece?.type).toBe('香')

    #たくさん参加してもエラーにならないことを確認
    20.times -> space.create_an_army user1, from_and_direction.direction, from_and_direction.from, 'horizontally'

describe 'Utils', ->
  it 'can judge objects are_same', ->
    obj1 = {}
    obj2 = {}
    expect(Utils.are_same(obj1, obj2)).toBe(true)
    obj1 = "ほげほげ"
    obj2 = "ほげほげ"
    expect(Utils.are_same(obj1, obj2)).toBe(true)
    obj1 = {}.toString()
    obj2 = {}.toString()
    expect(Utils.are_same(obj1, obj2)).toBe(true)
    obj1 = [1,2,"hoge"]
    obj2 = [1,2,"hoge"]
    expect(Utils.are_same(obj1, obj2)).toBe(true)
    obj1 = { hp: 10, name: 'hoge' }
    obj2 = { hp: 10, name: 'hoge'  }
    expect(Utils.are_same(obj1, obj2)).toBe(true)
    obj1 = { hp: 9,  name: 'hoge' }
    obj2 = { hp: 10, name: 'hoge'  }
    expect(Utils.are_same(obj1, obj2)).toBe(false)
    expect(Utils.are_same([], [])).toBe(true)

  it 'can judge object is same as before object', ->
    KEY = "same_object_judgment"
    obj1 =   { hp: 9,  name: 'hoge' }
    obj1_2 = { hp: 9,  name: 'hoge' }
    obj2 =   { hp: 10, name: 'hoge' }
    expect(Utils.is_same_as_before(KEY, obj1)).toBe(false)
    expect(Utils.is_same_as_before(KEY, obj1)).toBe(true)
    expect(Utils.is_same_as_before(KEY, obj1_2)).toBe(true)
    expect(Utils.is_same_as_before(KEY, obj2)).toBe(false)
    expect(Utils.is_same_as_before(KEY, obj2)).toBe(true)
    expect(Utils.is_same_as_before(KEY, obj2)).toBe(true)
    expect(Utils.is_same_as_before(KEY, obj2)).toBe(true)

describe 'Piece', ->
  it 'can get owner', ->
    space = new Space([20, 10])
    user = new User(space)
    piece = space.create_piece [3, 1], '金', user, 'd'
    expect(piece.owner()).toBe(user)
    
  it 'can change direction', ->
    space = new Space([20, 10])
    user = new User(space)
    piece = space.create_piece [3, 1], '金', user, 'd'
    piece.set_direction 'u'
    expect(piece.direction).toBe('u')
    piece.set_direction 'hoge'
    expect(piece.direction).toBe('u')
    piece.set_direction 'r'
    expect(piece.direction).toBe('u')
    spyOn(piece.owner(), 'is_available_time').andReturn(true)
    piece.set_direction 'r'
    expect(piece.direction).toBe('r')

describe 'PieceMovement', ->
  it 'can get movable xys of type', ->
    expect(PieceMovement.get_movable_xys_of_type('歩')).toEqual([[0,-1]])
    expect(PieceMovement.get_movable_xys_of_type('香')).toEqual [[
      [0,-1]
      [0,-2]
      [0,-3]
      [0,-4]
      [0,-5]
      [0,-6]
      [0,-7]
      [0,-8]
      [0,-9]
    ]]

  it 'can get abs xy', ->
    expect(PieceMovement.get_abs_xy([-1,-1], [10,10])).toEqual([9,9])
    expect(PieceMovement.get_abs_xy([-5,-3], [10,10])).toEqual([5,7])
    expect(PieceMovement.get_abs_xy([7,6], [4,2])).toEqual([11,8])

  describe 'roll()', ->
    describe '歩', ->
      fu_move = [0,-1]
      it 'can move to u', ->
        expect(PieceMovement.roll(fu_move, 'u')).toEqual([0,-1])
      it 'can move to r', ->
        expect(PieceMovement.roll(fu_move, 'r')).toEqual([1,0])
      it 'can move to d', ->
        expect(PieceMovement.roll(fu_move, 'd')).toEqual([0,1])
      it 'can move to l', ->
        expect(PieceMovement.roll(fu_move, 'l')).toEqual([-1,0])

    describe '桂馬', ->
      keima_move_right = [1,-2]
      it 'can keima_move_right to u', ->
        expect(PieceMovement.roll(keima_move_right, 'u')).toEqual([ 1,-2])
      it 'can keima_move_right to r', ->
        expect(PieceMovement.roll(keima_move_right, 'r')).toEqual([ 2, 1])
      it 'can keima_move_right to d', ->
        expect(PieceMovement.roll(keima_move_right, 'd')).toEqual([-1, 2])
      it 'can keima_move_right to l', ->
        expect(PieceMovement.roll(keima_move_right, 'l')).toEqual([-2,-1])

      keima_move_left = [-1,-2]
      it 'can keima_move_left to u', ->
        expect(PieceMovement.roll(keima_move_left, 'u')).toEqual([-1,-2])
      it 'can keima_move_left to r', ->
        expect(PieceMovement.roll(keima_move_left, 'r')).toEqual([ 2,-1])
      it 'can keima_move_left to d', ->
        expect(PieceMovement.roll(keima_move_left, 'd')).toEqual([ 1, 2])
      it 'can keima_move_left to l', ->
        expect(PieceMovement.roll(keima_move_left, 'l')).toEqual([-2, 1])

  describe 'get_xys_of_piece()', ->
    space = new Space([20, 10])
    user = new User(space)
    spyOn(user, 'is_available_time').andReturn(true)

    keima = space.create_piece [4, 7], '桂', user, 'u'
    keima.set_direction 'u'
    expect(PieceMovement.get_xys_of_piece_and_space(keima, space)).toEqual([[5,5], [3,5]])
    keima.set_direction 'r'
    expect(PieceMovement.get_xys_of_piece_and_space(keima, space)).toEqual([[6,8], [6,6]])
    keima.set_direction 'd'
    expect(PieceMovement.get_xys_of_piece_and_space(keima, space)).toEqual([[3,9], [5,9]])
    keima.set_direction 'l'
    expect(PieceMovement.get_xys_of_piece_and_space(keima, space)).toEqual([[2,6], [2,8]])

    fu = space.create_piece [4, 2], '歩', user, 'u'
    fu.set_direction 'u'
    expect(PieceMovement.get_xys_of_piece_and_space(fu, space)).toEqual([[4,1]])
    fu.set_direction 'r'
    expect(PieceMovement.get_xys_of_piece_and_space(fu, space)).toEqual([[5,2]])
    fu.set_direction 'd'
    expect(PieceMovement.get_xys_of_piece_and_space(fu, space)).toEqual([[4,3]])
    fu.set_direction 'l'
    expect(PieceMovement.get_xys_of_piece_and_space(fu, space)).toEqual([[3,2]])

  describe 'PieceMovement#get_xys_of_piece()', ->
    it 'Movable grids of 香車 is correct when there are some obstacles', ->
      space = new Space([20,20])
      user = new User(space)
      spyOn(user, 'is_available_time').andReturn(true);
      current_xy = [3, 3]
      kyo = space.create_piece current_xy, '香', user, 'u'
      expect(PieceMovement.get_xys_of_piece(kyo, current_xy, space.grids)).toEqual [
        [3,2]
        [3,1]
        [3,0]
      ]
      kyo.set_direction('d')
      expect(PieceMovement.get_xys_of_piece(kyo, current_xy, space.grids)).toEqual [
        [3,4]
        [3,5]
        [3,6]
        [3,7]
        [3,8]
        [3,9]
        [3,10]
        [3,11]
        [3,12]
      ]
      obstacle = space.create_piece [3,6], '香', user, 'd'
      expect(PieceMovement.get_xys_of_piece(kyo, current_xy, space.grids)).toEqual [
        [3,4]
        [3,5]
      ]
      space.put_piece obstacle.id, 3, 8
      expect(space.get_grid([3, 8]).piece.type).toBe('香')
      expect(space.get_grid([3, 6]).piece).toBe(null)
      expect(PieceMovement.get_xys_of_piece(kyo, current_xy, space.grids)).toEqual [
        [3,4]
        [3,5]
        [3,6]
        [3,7]
      ]

  describe 'set rular', ->
    it 'can set rular of an army', ->
      space = new Space([20,20])
      user = new User(space)
      space.create_an_army(user, 'd', [10, 10])
      expect(space.create_an_army(user, 'd', [10, 10]).length).toEqual(44)

  describe 'GAME OVER', ->
    it 'can judge GAME OVER', ->
      space = new Space([20,20])
      winner = new User(space)
      loser = new User(space)
      space.create_an_army(loser, 'd', [0, 0])

      expect(User.get_all_pieces(loser.id).length).toBe(20)

      expect(space.get_grid([1, 1]).piece?.type).toBe('香')
      expect(space.get_grid([1, 1]).piece?.owner()).toBe(loser)
      king = space.get_grid([5, 1]).piece
      king_id = king.id
      expect(king?.type).toBe('王')

      king_slayer = space.create_piece [5,2], '歩', winner, 'u'
      space.put_piece king_slayer.id, 5, 1
      expect(space.get_grid([5, 1]).piece?.type).toBe('歩')
      expect(space.get_grid([1, 1]).piece?.owner()).toBe(winner)

  describe 'get surrounding grids', ->
    space = new Space([20,20])
    xys = PieceMovement.surrounding_grids([1..1])
    expect(xys).toEqual([[-1,0], [0,1], [1,0], [0,-1]])
    xys = PieceMovement.surrounding_grids([1..2])
    expect(xys.length).toEqual(12)

    user = new User(space)
    space.create_an_army(user, 'd', [10, 10])

    expect(PieceMovement.get_grids_of_field_pieces(user, space.grids).length).toEqual(20)
    xys = PieceMovement.get_having_pieces_movable_xys(user, space.grids)
    expect(xys.length).toEqual(68)
