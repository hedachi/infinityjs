root = if global? then global else window

class PieceMovement
  @get_having_pieces_movable_xys = (user, grids) ->
    xys = []
    grids_of_field_pieces = @get_grids_of_field_pieces(user, grids)
    grids_of_field_pieces.forEach (grid) =>
      xys.push [grid.x, grid.x]
      xys = xys.concat(@get_movable_xys_by_absolute_xy([grid.x, grid.y], grids))
    xys = xys.filter (xy) ->
      grid = grids[xy[0]]?[xy[1]]
      grid && !grid.is_wall && !grid.piece?
    xys.unique()
  @get_grids_of_field_pieces = (user, grids) ->
    grids_of_field_pieces = root.User.get_all_pieces(user.id).filter((piece) =>
      !user.having_piece_ids.some(piece.id)
    ).map((piece) =>
      root.Grid.find_grid_by_piece_id(piece.id, grids)
    )
  @get_movable_xys_by_absolute_xy = (absolute_xy, grids) ->
    xys = (PieceMovement.surrounding_grids([1..3]).map((xy) ->
      [absolute_xy[0] + xy[0], absolute_xy[1] + xy[1]]
    ).filter((xy) =>
      grids[xy[0]]?[xy[1]]?)
    )
  @surrounding_grids = (ranges = [1..1]) ->
    _surrounding_xys = new Array
    for range in ranges
      for xy in @xy_of_range(range)
        _surrounding_xys.push(xy)
    _surrounding_xys
  @xy_of_range = (range = 1) ->
    results = new Array
    x = 0
    y = -range
    x_sign = -1
    y_sign = -1
    (range * 4).times (num) =>
      if Math.abs(x) == range
        x_sign *= -1
      if Math.abs(y) == range
        y_sign *= -1
      x += x_sign
      y += y_sign
      results.push [x, y]
    results
  @get_xys_of_piece_and_space = (piece, space) ->
    grid = root.Grid.find_grid_by_piece_id(piece.id, space.grids)
    piece_current_abs_xy = [grid.x, grid.y]
    @get_xys_of_piece(piece, piece_current_abs_xy, space.grids)
  @get_xys_of_piece = (piece, piece_current_abs_xy, grids) ->
    if @is_using_special_movement_logic(piece)
      abs_xys = []
      @get_movable_xys_of_type(piece.type, piece.is_promoted).forEach (movable_xy_group) =>
        this_group_is_available = true
        movable_xy_group.forEach (movable_xy_actual) =>
          if this_group_is_available
            xy = @get_abs_xy(@roll(movable_xy_actual, piece.direction), piece_current_abs_xy)
            grid = grids[xy[0]]?[xy[1]]
            unless grid
              this_group_is_available = false
            else
              switch grid.can_put(piece)
                when false
                  this_group_is_available = false
                when 'move'
                  abs_xys.push xy
                when 'attack'
                  abs_xys.push xy
                  this_group_is_available = false
      abs_xys
    else
      (@get_movable_xys_of_type(piece.type, piece.is_promoted).map (movable_xy) =>
        @get_abs_xy(@roll(movable_xy, piece.direction), piece_current_abs_xy)
      ).filter (xy) ->
        grids[xy[0]]?[xy[1]]?.can_put(piece)
  @is_using_special_movement_logic = (piece) ->
    piece.type == '飛' || piece.type == '角' || (piece.type == '香' && !piece.is_promoted)
  @get_movable_xys_of_type = (type, is_promoted) ->
    switch type
      when '歩'
        unless is_promoted
          [[0,-1]]
        else
          @XYS_GOLD
      when '香'
        unless is_promoted
          [
            @XYS_FORWARD
          ] 
        else
          @XYS_GOLD
      when '桂'
        unless is_promoted
          [
            [ 1,-2]
            [-1,-2]
          ]
        else
          @XYS_GOLD
      when '飛'
        unless is_promoted
          [
            @XYS_FORWARD
            @XYS_BACK
            @XYS_LEFT
            @XYS_RIGHT
          ]
        else
          [
            @XYS_FORWARD
            @XYS_BACK
            @XYS_LEFT
            @XYS_RIGHT
            [[-1,-1]]
            [[ 1,-1]]
            [[-1, 1]]
            [[ 1, 1]]
          ]
      when '角'
        unless is_promoted
          [
            @XYS_FORWARD_LEFT
            @XYS_FORWARD_RIGHT
            @XYS_BACK_LEFT
            @XYS_BACK_RIGHT
          ]
        else
          [
            @XYS_FORWARD_LEFT
            @XYS_FORWARD_RIGHT
            @XYS_BACK_LEFT
            @XYS_BACK_RIGHT
            [[-1, 0]]
            [[ 1, 0]]
            [[ 0, 1]]
            [[ 0,-1]]
          ]
      when '金'
        @XYS_GOLD
      when '銀'
        unless is_promoted
          [
            [ 0,-1]
            [-1,-1]
            [ 1,-1]
            [-1, 1]
            [ 1, 1]
          ]
        else
          @XYS_GOLD
      when '王'
        [
          [-1,-1]
          [ 0,-1]
          [ 1,-1]
          [ 1, 0]
          [ 1, 1]
          [ 0, 1]
          [-1, 1]
          [ 1, 1]
          [-1, 0]
        ]
      else
        []
  @get_abs_xy = (relative_xy, absolute_xy) ->
    [absolute_xy[0] + relative_xy[0], absolute_xy[1] + relative_xy[1]]
  @roll = (relative_xy, direction) ->
    if direction == 'u'
      x = relative_xy[0]
      y = relative_xy[1]
    if direction == 'r'
      x = - 1 * relative_xy[1]
      y = relative_xy[0]
    if direction == 'l'
      x = relative_xy[1]
      y = - 1 * relative_xy[0]
    if direction == 'd'
      x = - 1 * relative_xy[0]
      y = - 1 * relative_xy[1]
    [x,y]
  @XYS_FORWARD = [
    [0,-1]
    [0,-2]
    [0,-3]
    [0,-4]
    [0,-5]
    [0,-6]
    [0,-7]
    [0,-8]
    [0,-9]
  ]
  @XYS_LEFT = [
    [-1,0]
    [-2,0]
    [-3,0]
    [-4,0]
    [-5,0]
    [-6,0]
    [-7,0]
    [-8,0]
    [-9,0]
  ]
  @XYS_RIGHT = [
    [1,0]
    [2,0]
    [3,0]
    [4,0]
    [5,0]
    [6,0]
    [7,0]
    [8,0]
    [9,0]
  ]
  @XYS_BACK = [
    [0,1]
    [0,2]
    [0,3]
    [0,4]
    [0,5]
    [0,6]
    [0,7]
    [0,8]
    [0,9]
  ]
  @XYS_FORWARD_LEFT = [
    [-1,-1]
    [-2,-2]
    [-3,-3]
    [-4,-4]
    [-5,-5]
    [-6,-6]
    [-7,-7]
    [-8,-8]
    [-9,-9]
  ]
  @XYS_FORWARD_RIGHT = [
    [1,-1]
    [2,-2]
    [3,-3]
    [4,-4]
    [5,-5]
    [6,-6]
    [7,-7]
    [8,-8]
    [9,-9]
  ]
  @XYS_BACK_LEFT = [
    [-1,1]
    [-2,2]
    [-3,3]
    [-4,4]
    [-5,5]
    [-6,6]
    [-7,7]
    [-8,8]
    [-9,9]
  ]
  @XYS_BACK_RIGHT = [
    [1,1]
    [2,2]
    [3,3]
    [4,4]
    [5,5]
    [6,6]
    [7,7]
    [8,8]
    [9,9]
  ]
  @XYS_GOLD = [
    [-1,-1]
    [ 0,-1]
    [ 1,-1]
    [-1, 0]
    [ 1, 0]
    [ 0, 1]
  ]
  
root.PieceMovement = PieceMovement
