root = if global? then global else window
l = console.log

class Space
  constructor: (end_xy = [0, 0]) -> 
    @grids = []
    @start_xy = [0, 0]
    @end_xy = end_xy
    @create_grids()
    @users = []
  get_grid: (xy)->
    @grids[xy[0]]?[xy[1]] || throw "grid (#{xy[0]}, #{xy[1]}) is not found..."
  expand: (end_xy) -> 
    @end_xy = end_xy
    @create_grids()
  x_length: ->
    @end_xy[0] - @start_xy[0] + 1
  y_length: ->
    @end_xy[1] - @start_xy[1] + 1
  get_territory_grids_of: (user) ->
    @get_all_grids_as_array().filter (grid) ->
      user.id == grid.rular_id
  get_all_grids_as_array: ->
    grids = []
    @grids.forEach (grids_col) ->
      grids_col.forEach (grid) ->
        grids.push grid
    grids
  get_grid_rows: ->
    grid_rows = []
    @y_length().times (y) =>
      @grids.forEach((grids_col) -> grids_col)
      grid_rows.push @grids.map((grids_col) -> grids_col[y])
    grid_rows
  show_console: ->
    @get_grid_rows().forEach (grid_row) ->
      l grid_row.join("")
  to_json: ->
    type: 'space'
    grids: @grids
    users: @users
    pieces: root.DB.id_piece_map
  create_grids: ->
    for x in [@start_xy[0]..@end_xy[0]]
      @grids[x] ?= []
      for y in [@start_xy[1]..@end_xy[1]]
        @grids[x][y] ?= new root.Grid(x, y, @)
  count_grid: ->
    (@grids.map (grid_col) -> grid_col.length).sum()
  find_piece_by_id: (piece_id) ->
    root.DB.id_piece_map[piece_id]
  create_piece: (xy, type, owner, direction) ->
    console.log "create_piece: #{type} #{direction} (#{xy[0]},#{xy[1]})"
    unless xy
      throw "argument xy is undefined"
    piece = new root.Piece
      type: type
      owner: owner
      direction: direction
    try
      @get_grid(xy).put piece
    catch e
      console.log "ERROR: (#{xy[0]},#{xy[1]})"
      throw e
    piece
  put_piece: (piece_id, x, y) ->
    if piece_id == undefined || x == undefined || y == undefined
      console.log "invalid arguments: #{arguments}"
    piece = @find_piece_by_id piece_id
    destination_grid = @get_grid([x, y])
    if !piece.owner().is_available_time()
      console.log "ERROR: #{piece.owner().id} is not ready to put next piece!"
      false
    else if piece.is_living && !root.PieceMovement.get_xys_of_piece_and_space(piece, @).find((xy) -> xy[0] == x && xy[1] == y)
      console.log "ERROR: #{piece.type} can't move to (#{x}, #{y})..."
      console.log root.PieceMovement.get_xys_of_piece_and_space(piece, @)
      false
    else if !piece.is_living && !root.PieceMovement.get_having_pieces_movable_xys(piece.owner(), @grids)
      console.log "ERROR: #{piece.type} can't put to (#{x}, #{y})..."
      false
    else if destination_grid.can_put(piece)
      previous_grid = root.Grid.find_grid_by_piece_id(piece_id, @grids)
      if previous_grid
        previous_grid.piece = null
      else
        piece.owner().having_piece_ids.remove(piece.id)
      slain_piece = destination_grid.piece
      slain_piece_owner = slain_piece?.owner()
      destination_grid.put piece
      if previous_grid
        user_wants_to_promote = true
        if user_wants_to_promote
          piece.promote_if_can(previous_grid, destination_grid)
      piece.owner().last_put_time = new Date
      from = ""
      doing = ""
      if !previous_grid
        from = "手駒から"
        doing = "配置しました。"
      else if slain_piece
        doing = "移動して、#{slain_piece_owner.name_html_tag()}の#{slain_piece.type}を奪取しました。"
      else
        doing = "移動しました。"
      "#{piece.owner().name_html_tag()}は#{from}#{piece.type}を(#{destination_grid.x}, #{destination_grid.y})に#{doing}"
    else
      false
      console.log "ERROR: can't put #{piece.id} on that destination grid"
  get_empty_area_for_horizontally_long_mode: ->
    found = null
    10.times (i) =>
      unless found
        is_top = i % 2 == 0
        xy_from = [i * 5, (if is_top then 0 else 6)]
        xy_to = [xy_from[0] + 9, xy_from[1] + 2]
        if @search_piece(xy_from, xy_to).length == 0
          found =
            from: xy_from
            direction: (if is_top then "d" else "u")
    found || throw "empty area not found!"
  get_empty_area: ->
    found = null
    20.times (i) =>
      unless found
        ((i+1)*(i+1) - (i*i)).times (ii) =>
          unless found
            if ii <= (i/2) + 1
              x = i
              y = ii
            else
              x = i * 2 - ii
              y = i
            map = @start_area_coordinate_map()
            xy_from = [map[x][0], map[y][0]]
            xy_to = [map[x][1], map[y][1]]
            if @search_piece(xy_from, xy_to).length == 0
              found =
                from: xy_from
                to: xy_to
    found || throw "empty area not found!"
  start_area_coordinate_map : ->
    if @_start_area_coordinate_map
      @_start_area_coordinate_map
    else
      map = {}
      10.times (i) ->
        map[i] = [i*11, (i*11+10)]
      @_start_area_coordinate_map = map
  search_piece: (xy_from, xy_to) ->
    pieces = []
    [xy_from[0]..xy_to[0]].forEach (x) =>
      [xy_from[1]..xy_to[1]].forEach (y) =>
        piece = @grids[x]?[y]?.piece
        if piece then pieces.push(piece)
    pieces
  @calculate_relative_xy = (direction, xy, xy_default, mode) ->
    if mode == 'normal'
      if direction == 'd'
        x = xy[0] + xy_default[0] + 5
        y = xy[1] + xy_default[1] + 5
      if direction == 'l'
        x = - xy[1] + xy_default[0] + 5
        y = - xy[0] + xy_default[1] + 5
      if direction == 'r'
        x = xy[1] + xy_default[0] + 5
        y = xy[0] + xy_default[1] + 5
      if direction == 'u'
        x = - xy[0] + xy_default[0] + 5
        y = - xy[1] + xy_default[1] + 5
    else if mode == 'horizontally'
      if direction == 'd'
        x = xy[0] + xy_default[0] + 4
        y = xy[1] + xy_default[1] + 4
      if direction == 'u'
        x = - xy[0] + xy_default[0] + 4
        console.log "xy[1]:#{xy[1]}, xy_default[1]:#{xy_default[1]}"
        y = - xy[1] + xy_default[1] - 2
    [x,y]
  create_an_army: (user, direction, xy_default, mode = 'normal') ->
    if mode == 'normal'
      @end_xy = [Math.max(@end_xy[0], xy_default[0] + 10), Math.max(@end_xy[1], xy_default[1] + 10)]
    else if mode == 'horizontally'
      @end_xy = [Math.max(@end_xy[0], xy_default[0] + 9), 8]
    @create_grids()
    @create_pieces_of_an_army(user, direction, xy_default, mode)
    @create_walls_of_an_army(user, direction, xy_default, mode)
    @set_rular_of_an_army(user, direction, xy_default, mode)
  set_rular_of_an_army: (user, direction, xy_default, mode) ->
    if mode == 'normal'
      xys = []
      [-5..-2].forEach (y) ->
        [-5..5].forEach (x) ->
          xys.push [x, y]
    else if mode == 'horizontally'
      xys = []
      [-4..-2].forEach (y) ->
        [-4..4].forEach (x) ->
          xys.push [x, y]
    xys.map (xy) =>
      absolute_xy = Space.calculate_relative_xy(direction, xy, xy_default, mode)
      @get_grid(absolute_xy).rular_id = user.id
  create_walls_of_an_army: (user, direction, xy_default, mode) ->
    if mode == 'normal'
      walls_xy = Space.WALLS_BOTTOM.concat(Space.WALLS_LEFT).concat(Space.WALLS_RIGHT)
    else if mode == 'horizontally'
      if direction == 'd'
        walls_xy = Space.WALLS_RIGHT_FOR_HORIZONTALLY
      else if direction == 'u'
        walls_xy = Space.WALLS_RIGHT_FOR_HORIZONTALLY.concat(Space.WALLS_LEFT_FOR_HORIZONTALLY)
    walls_xy.forEach (xy) =>
      absolute_xy = Space.calculate_relative_xy(direction, xy, xy_default, mode)
      grid = @get_grid(absolute_xy)
      grid.is_wall = true
  create_pieces_of_an_army: (user, direction, xy_default, mode) ->
    put = (type, xy) =>
      xy = Space.calculate_relative_xy(direction, xy, xy_default, mode)
      console.log "call create_piece (#{xy[0]}, #{xy[1]})"
      @create_piece xy, type, user, direction
    put('歩', [-4, -2])
    put('歩', [-3, -2])
    put('歩', [-2, -2])
    put('歩', [-1, -2])
    put('歩', [ 0, -2])
    put('歩', [ 1, -2])
    put('歩', [ 2, -2])
    put('歩', [ 3, -2])
    put('歩', [ 4, -2])
    put('飛', [-3, -3])
    put('角', [ 3, -3])
    put('香', [-4, -4])
    put('桂', [-3, -4])
    put('銀', [-2, -4])
    put('金', [-1, -4])
    put('王', [ 0, -4])
    put('金', [ 1, -4])
    put('銀', [ 2, -4])
    put('桂', [ 3, -4])
    put('香', [ 4, -4])
  @WALLS_BOTTOM = [
    [-4,-5]
    [-3,-5]
    [-2,-5]
    [-1,-5]
    [ 0,-5]
    [ 1,-5]
    [ 2,-5]
    [ 3,-5]
    [ 4,-5]
  ]
  @WALLS_LEFT = [
    [-5,-5]
    [-5,-4]
    [-5,-3]
    [-5,-2]
  ]
  @WALLS_RIGHT = [
    [ 5,-5]
    [ 5,-4]
    [ 5,-3]
    [ 5,-2]
  ]
  @WALLS_RIGHT_FOR_HORIZONTALLY = [
    [ 5,-4]
    [ 5,-3]
    [ 5,-2]
  ]
  @WALLS_LEFT_FOR_HORIZONTALLY = [
    [-5,-4]
    [-5,-3]
    [-5,-2]
  ]

root.Space = Space
