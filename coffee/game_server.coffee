root = if global? then global else window

root.DB =
  id_user_map: {}
  id_piece_map: {}

modules_sub_path = '/Users/hedachi/node_modules'
require "#{modules_sub_path}/sugar"
require './space'
require './piece'
require './grid'
require './utils'
require './config'
require './piece_movement'

class GameServer
  @DIRECTIONS = ["r", "d", "u", "r"]
  @DIRECTIONS_HORIZONTALLY = ["d","u","d","u","d","u","d","u","d","u","d","u","d","u","d","u","d","u","d","u"]
  constructor: ->
    @space = new root.Space
  get_space: (params, cb) ->
    try
      space = JSON.stringify(@space.to_json())
    catch e
      console.log e
      console.log @space
      throw e
    cb(space)
  expand_space: (params, cb) ->
    @space.expand(params.xy)
  join: (params, cb) ->
    user = params.user
    unless user.is_joining
      MODE = 'horizontally' #'normal'
      if MODE == 'normal'
        init_position = @space.get_empty_area().from
        direction = GameServer.DIRECTIONS.shift() || ["r", "u", "d", "l"].sample()
      else if MODE == 'horizontally'
        from_and_direction = @space.get_empty_area_for_horizontally_long_mode()
        init_position = from_and_direction.from
        direction = from_and_direction.direction
      @space.create_an_army(user, direction, init_position, MODE)
      user.is_joining = true
    cb JSON.stringify
      type: 'join'
      text: "hoge"
  create_space: (params, cb) ->
  destroy_space: (params, cb) ->
    throw 'destroy'
  put_piece: (params, cb) ->
    piece = @space.find_piece_by_id params.piece_id
    if piece && piece.owner() == params.user
      message = @space.put_piece piece.id, params.x, params.y
    cb JSON.stringify
      type: 'put_piece'
      message: message
      deselect: params.user.id
  change_direction : (params, cb) ->
    if params.piece_id == undefined || !root.Piece.is_valid_direction(params.direction)
      console.log "ERROR: arguments invalid #{params}"
    piece = root.Piece.get_by_id(params.piece_id)
    is_success = piece.set_direction(params.direction)
    if is_success
      message = "#{params.user.name_html_tag()}は#{piece.type}を#{Piece.DIRECTIONS[params.direction]}に方向転換した。"
    cb JSON.stringify
      type: 'change_direction'
      message: message
      deselect: params.user.id
  change_name : (params, cb) ->
    previous_name = params.user.name_html_tag()
    params.user.set_name(params.name)
    cb JSON.stringify
      type: 'change_direction'
      message: "#{previous_name}の名前は#{params.user.name_html_tag()}に変更されました。"
  chat : (params, cb) ->
    cb JSON.stringify
      type: 'chat'
      message: "#{params.user.name_html_tag()}: #{params.message}"

module.exports = GameServer
