root = if global? then global else window
root.after = (millisecond, func) -> setTimeout func, millisecond

DB =
  id_user_map: {}
  id_piece_map: {}
root.DB = DB

class NetworkClient
  pieces: {}
  init_websocket: ->
    unless @ws
      @ws = new WebSocket('ws://127.0.0.1:7777/websocket')
      @ws.onopen = =>
        @log 'connection opened.'
      @ws.onclose = (event) =>
        if (event) then @log(event)
        @log('close.')
      @ws.onmessage = (event) =>
        try
          if !event.data
            console.log 'event has no data'
          else
            data = JSON.parse(event.data)
            if data.deselect == user?.id
              UIEventController.selecting_piece_id = null
              $('div.piece.selected').removeClass("selected")
              $("div.grid.movable").removeClass 'movable'
            if data.type == 'space'
              grids_data = GridC.attach_prototype(data.grids)
              GridC.set_grids_data(grids_data)
              Space.show(grids_data)
              UserC.update_map(data.users)
              PieceC.update_map(data.pieces)
            else if data.type == 'join'
              root.user = new UserC(data.user)
            else
              console.log("received a message:")
              console.log(data)
            if data.message
              $html = $("<div>#{data.message}</div>")
              $html.find('.user_name').each (index, user_name_dom) ->
                $user_name = $(user_name_dom)
                color = DB.id_user_map[$user_name.data('user_id')]?.color
                $user_name.css { background: color }
              $("div#news").prepend($html)
        catch exception
          console.log event.data
          throw exception
      @ws.onerror = (event) =>
        @log('ERROR!')
  send: ->
    @ws.send($('#sended_value').val())
  connect: ->
    @init_websocket()
  close: ->
    @ws.close()
    @ws = null
  send_command: (json) ->
    @ws.send(JSON.stringify(json))
  log: (text) ->
    console.log(text)

class UserC
  constructor: (user_data) ->
    @id = user_data.id
    @update_data(user_data)
  update_data: (user_data) ->
    $('span#user_name').html(@id)
    @having_piece_ids = user_data.having_piece_ids
    @show_having_pieces()
    @last_put_time = Date.create(user_data.last_put_time).getTime()
    $('span#user_name').css
      background: user_data.color
    if user_data.is_joining then $("input#join_button").hide() else $("input#join_button").show()
  update_waiting_time: () ->
    HAVE_TO_WAIT_SECOND = Config.waiting_second
    now = (new Date).getTime()
    waiting_second = HAVE_TO_WAIT_SECOND - Math.ceil((now - @last_put_time) / 1000)
    if waiting_second < 0
      $('div#availability').html("次の手を指せます！！！！")
      $('div#user_status').removeClass 'waiting'
    else
      $('div#availability').html("あと #{waiting_second} 秒お待ち下さい...")
      $('div#user_status').addClass 'waiting'
  having_pieces: () ->
    @having_piece_ids.map (piece_id) =>
      DB.id_piece_map[piece_id]
  show_having_pieces: () ->
    if !Utils.is_same_as_before("Client#show_having_pieces", @having_pieces())
      setTimeout =>
        $('div#having_pieces').empty()
        @having_piece_ids.forEach (piece_id) ->
          $('div#having_pieces').append(PieceC.create_by_id(piece_id))
      , 500
  @show_all_users_information = ->
    $("div#users").html("参加者: ")
    Object.values(DB.id_user_map).forEach (user) ->
      $("div#users").append(" <span style='background-color:#{user.color}'>#{user.id}</span> ")
  @update_map = (user_datas) ->
    if !Utils.is_same_as_before("UserC#update_map_all", user_datas)
      user_datas.forEach (user_data) =>
        DB.id_user_map[user_data.id] = user_data
      @show_all_users_information()
    @update_current_user(user_datas)
  @update_current_user = (user_datas) ->
    if user_datas
      user_datas.forEach (user_data) =>
        if user_data.id == user.id
          if !Utils.is_same_as_before("UserC#update_map_current_user", user_data)
            user.update_data(user_data)
  @schedule_update_waiting_time = ->
    setInterval (-> user.update_waiting_time()), 200

root.UserC = UserC

class UIEventController
  @selecting_piece_id = null
  @attach_events = ->
    $(document).on 'click', 'div.grid', (event) =>
      $target = $(event.target)
      console.log "grid clicked event_target_id: #{$target.attr('id')}"
      unless $target.hasClass('grid')
        $target = $target.parent()
      #if $target.hasClass('grid movable') || $('div.piece.selected').parent().attr('id') == 'having_pieces'
      if $target.hasClass('grid movable')
        console.log "$target.hasClass('grid movable')"
        network_client.send_command
          command: 'put_piece'
          params:
            piece_id: @selecting_piece_id
            x: $target.data('x')
            y: $target.data('y')
      else
        console.log $target
      return false #to stop event propergation(bubbling?)
    $(document).on 'click', 'div.piece', (event) =>
      console.log 'piece clicked'
      $target_piece = $(event.target)
      is_owners_piece = $target_piece.data('owner_id') == user.id
      if $target_piece.attr('id') == @selecting_piece_id
        $('div.piece.selected').removeClass("selected")
        PieceC.hide_movable_xys()
        @selecting_piece_id = null
        false
      else if !is_owners_piece && @selecting_piece_id
        true
      else if is_owners_piece
        PieceC.show_movable_xys($target_piece)
        $('div.piece.selected').removeClass("selected")
        @selecting_piece_id = $target_piece.attr('id')
        $target_piece.addClass("selected")
        false
      else
        PieceC.show_movable_xys($target_piece)
        true
    $(document).on 'click', 'div.arrow', (event) =>
      console.log 'arrow clicked'
      if @selecting_piece_id
        network_client.send_command
          command: 'change_direction'
          params:
            piece_id: @selecting_piece_id
            direction : $(event.target).data('direction')
    $(document).on 'click', 'input#join_button', (event) =>
      network_client.send_command
        command: 'join'
    $(document).on 'click', 'input#server_error_button', (event) =>
      network_client.send("えらー")
    $(document).on 'click', 'input#change_name', (event) =>
      name = prompt '名前を入力して下さい'
      if name
        network_client.send_command
          command: 'change_name'
          params:
            name: name
    $(document).on 'click', 'input#send_message', (event) =>
      text = $('input#message').val()
      if text
        $('input#message').val("")
        network_client.send_command
          command: 'chat'
          params:
            message: text

class GridC
  @grids_data = null
  @attach_prototype = (grids_data) ->
    grids_data.map (grids_group) ->
      grids_group.map (grid_data) ->
        new_data = Object.clone(grid_data)
        new_data.__proto__ = Grid.prototype
        new_data.piece?.__proto__ = Piece.prototype
        new_data
  @set_grids_data = (grids_data) ->
    @grids_data = grids_data
  @exists = (id) ->
    $("##{id}").size() > 0
  @get_id = (grid_data) ->
    "grid_#{grid_data.x}_#{grid_data.y}"
  @get_id_by_xy = (xy) ->
    "grid_#{xy[0]}_#{xy[1]}"
  @create = (grid_data) ->
    id = @get_id(grid_data)
    $grid = $("<div class='grid' id='#{id}'data-x='#{grid_data.x}' data-y='#{grid_data.y}'></div>")
    $grid.css @get_position([grid_data.x, grid_data.y])
    $('#board').append($grid)
    $grid.append("<div class='flag'></div>")
    $grid
  @get_position = (xy) ->
    top:  "#{xy[1] * 40}px"
    left: "#{xy[0] * 40}px"

root.GridC = GridC

class Space
  @show = (space_data) ->
    space_data.forEach (space_row_data, x) =>
      space_row_data.forEach (grid_data, y) =>
        grid_id = GridC.get_id(grid_data)
        if GridC.exists(grid_id)
          $grid = $("##{grid_id}")
        else
          $grid = GridC.create(grid_data)
        if grid_data.is_wall
          $grid.addClass 'wall'
        $piece = $grid.find("div.piece")
        if !grid_data.piece || grid_data.piece.id != $piece.attr('id')
          $grid.find("div.piece").detach()
        if grid_data.piece && !Utils.is_same_as_before("Space#show_#{x}_#{y}", grid_data)
          $("##{grid_data.piece.id}").detach()
          $grid.append(PieceC.create(grid_data.piece))
        if grid_data.rular_id
          owner = -> DB.id_user_map[grid_data.rular_id]
          $flag = $("##{grid_id} div.flag")
          if owner()
            $("##{grid_id} div.flag").css
              "background-color": owner().color
          else
            after 200, -> $("##{grid_id} div.flag").css
              "background-color": owner().color
        #if !Utils.is_same_as_before("Space#rular_of_#{x}_#{y}", grid_data.rular_id)

class PieceC
  @create = (piece_data) ->
    $piece = $("<div class='piece'>#{piece_data.type}</div>")
    owner = -> DB.id_user_map[piece_data.owner_id]
    if owner()
      $piece.css
        background: owner().color
    else
      after 200, -> $piece.css
        background: owner().color
    $piece.attr 'data-owner_id', piece_data.owner_id
    Utils.rotate $piece, piece_data.direction
    $piece.attr 'id', piece_data.id
    if piece_data.is_promoted
      $piece.addClass 'promoted'
    $piece
  @create_by_id = (id) ->
    piece_data = DB.id_piece_map[id]
    if piece_data
      @create(piece_data)
    else
      console.log "piece_data is nothing"
  @update_map = (id_piece_map) ->
    $.each id_piece_map, (id, piece) =>
      piece.__proto__ = Piece.prototype
      piece.is_made_in_PieceC_update_map = true
      DB.id_piece_map[id] = piece
  @hide_movable_xys = () ->
    $("div.grid.movable").removeClass 'movable'
  @show_movable_xys = ($target_piece) ->
    piece = DB.id_piece_map[$target_piece.attr('id')]
    $parent_grid = $target_piece.parent()
    xy = [$parent_grid.data('x'), $parent_grid.data('y')]
    console.log xy
    @hide_movable_xys()
    is_field_piece = $parent_grid.hasClass("grid")
    if is_field_piece
      xys = PieceMovement.get_xys_of_piece(piece, xy, GridC.grids_data)
      console.log 'hoge'
    else
      xys = PieceMovement.get_having_pieces_movable_xys(root.user, GridC.grids_data)
      console.log xys
    xys.forEach (xy) ->
      grid_id = GridC.get_id_by_xy(xy)
      $("##{grid_id}").addClass 'movable'

root.PieceC = PieceC

class Development
  @create_buttons = ->
    $.each({
      #connect: {}
      #close: {}
      #create_space: {}
      #destroy_space: {}
      #expand_space: {xy: [12,12]}
      #join: {}
    },(action, value) ->
      $div = $("<div></div>")
      $button = $("<input type='button' value='#{action}' id='#{action}' />")
      $button.click =>
        if network_client[action]
          network_client[action]()
        else
          network_client.send_command
            command: action
            params: JSON.parse($("##{action}_value").val())
      $div.append($button)
      $div.append("<input type='text' value='#{JSON.stringify(value)}' id='#{action}_value' />")
      $('#ui_dev').append($div)
    )

class UIAdjuster
  @grid_height = null
  @margin_for_ui = 100
  @run = -> setTimeout UIAdjuster.set_board_height, 200
  @set_board_height = ->
    UIAdjuster.grid_height = $("div.grid:first").height()
    unless UIAdjuster.grid_height
      UIAdjuster.run()
    else
      height = GridC.grids_data[0].length * UIAdjuster.grid_height + UIAdjuster.margin_for_ui + 'px'
      console.log "set height: #{height}"
      $("div#board").css 'height', height
      setTimeout UIAdjuster.set_board_height, 5000

$ ->
  div = $('body').append($('<div />'))
  network_client = new NetworkClient
  root.network_client = network_client
  UIEventController.attach_events()
  Development.create_buttons()
  network_client.connect()
  UIAdjuster.run()
  UserC.schedule_update_waiting_time()
