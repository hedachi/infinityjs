root = if global? then global else window

class Grid
  constructor : (x, y, space) ->
    @id = "g_" + root.Utils.create_id()
    @x = x
    @y = y
    @piece = null
    @is_wall = false
    @rular_id = null
    unless space then throw "space is nessecary!"
    @space = space
  toJSON: ->
    hash = 
      id: @id
      x: @x
      y: @y
      piece: @piece
      is_wall: @is_wall
      rular_id: @rular_id
  put: (coming_piece) ->
    availability = @can_put(coming_piece)
    if availability == 'move'
      @piece = coming_piece
      if !coming_piece.is_living
        @piece.revive()
      true
    else if availability == 'attack'
      coming_piece.owner().gain_piece(@piece, @space)
      @piece = coming_piece
      true
    else
      console.log "#{@piece.name()} is here, so can't put #{coming_piece.name()}"
      false
  can_put: (coming_piece) ->
    if @is_wall
      false
    else if !@piece
      'move'
    else if @piece.owner() != coming_piece.owner() && coming_piece.is_living
      'attack'
    else
      false
  @find_grid_by_piece_id = (piece_id, grids) ->
    found = null
    grids.forEach (grids_col, x) ->
      unless found
        grids_col.forEach (grid, y) ->
          if grid.piece?.id == piece_id
            found = grid
    found

root.Grid = Grid
