root = if global? then global else window

fs = require 'fs'
http = require 'http'
WebSocket = require 'ws'
modules_sub_path = '../node_modules'
require "#{modules_sub_path}/sugar"

Timer = require './timer'
GameServer = require './game_server'
require './user'

IGNORE_URI = ['favicon.ico']

common_configs = 
  port: 7777

server_configs = 
  host: '127.0.0.1'

class Server
  constructor : -> 
    @server = null
    @game_server = new GameServer
    @wsConnections = []
    @free_users = []
  start : ->
    @server = http.createServer @handle_request.bind(@)
    @server.listen common_configs.port, server_configs.host
    @init_ws()
    #@schedule_update()
  schedule_update: ->
    setInterval @update.bind(@), 5000
  update: ->
    @game_server.get_space {}, (json) =>
      @broadcast(json)
  broadcast: (message) ->
    @wsConnections.forEach (socket) ->
      if socket.readyState == WebSocket.OPEN
        socket.send message
  broadcast_connected_sockets: (message) ->
    @broadcast('[connected sockets]' + @wsConnections.length)
  init_ws : ->
    @wsServer = new WebSocket.Server
      server: @server
      path: '/websocket'
    console.log "game server started listening on port:#{common_configs.port} ..."
    @wsServer.on 'connection', (ws) =>
      console.log("WebSocketServer connected")
      @wsConnections.push(ws) # 配列にソケットを格納
      user = @free_users.pop()
      unless user
        user = new root.User(@game_server.space)
        IS_AUTO_JOINING = false
        if IS_AUTO_JOINING
          @game_server.join { user: user }, (res) -> console.log(res)
      user.on_connection_opened()
      user.is_connected = true
      ws.on 'message', (message) =>
        try
          console.log('receive request: ' + message)
          json = JSON.parse(message)
          if !json.params
            params = { user: user }
          else
            params = Object.merge(json.params, { user: user })
          if @game_server[json.command]
            @game_server[json.command](params, (ret_string) =>
              @broadcast(ret_string)
            )
            @update()
        catch e
          console.log "############################################################"
          console.log "ERROR REPORT: " + new Date
          console.log "------------------------------------------------------------"
          console.log e.stack
          console.log "############################################################"
      ws.on 'close', =>
        console.log('stopping client send "close"')
        user.on_connection_closed()
        @free_users.push user
        @wsConnections = @wsConnections.filter (conn, i) -> conn != ws # 接続切れのソケットを配列から除外
      if ws.readyState == WebSocket.OPEN
        ws.send JSON.stringify({
          type: "join"
          user: user
        })
        @update()
  handle_request : (req, res) ->
    console.log "called for #{req.url}"
    timer = Timer.create_and_start()
    results = {}
    try
      @execute req.url, (res_body) ->
        console.log "Server#execute() finished."
        timer.show()
        if res
          res.writeHead 200, {'Content-Type':'application/json'}
          res.end res_body
        else
          results = res_body
    catch error
      console.log error.stack
      if res
        res.writeHead 200, {'Content-Type':'application/json'}
        res.end '{"result":"error"}'
    results
  execute : (url, cb) ->
    commands = url.split('?').first().split('/')
    command = commands.last()
    params = @get_parameters(url)
    if IGNORE_URI.find((url) -> url == command)
      console.log "not allowd."
      cb("uho.")
    else
      @game_server[command](params, cb)
  get_parameters : (url) ->
    params_strings = url.split('?')[1]
    params = []
    params_strings?.split('&').each (str) ->
      params[str.split('=')[0]] = str.split('=')[1]
    params

root.Server = Server
