root = if global? then global else window

class Utils
  @_is_same_as_before_cache = {}
  @create_id = () ->
    Math.floor(Math.random() * 1000000000000)
  @rand = (num) ->
    Math.floor(Math.random() * num)
  @percent = (num) ->
    Math.floor(Math.random() * 10000) < num * 100
  @are_same = (obj1, obj2) ->
    if obj1 == obj2
      true
    else if obj1 instanceof Array && obj2 instanceof Array
      obj1.join('_') == obj2.join('_')
    else
      JSON.stringify(obj1) == JSON.stringify(obj2)
  @is_same_as_before = (key_string, data) ->
    if @_is_same_as_before_cache[key_string] == undefined
      @_is_same_as_before_cache[key_string] = data
      false
    else if @are_same(@_is_same_as_before_cache[key_string], data)
      true
    else
      @_is_same_as_before_cache[key_string] = data
      false

  @rotate = ($jQueryObject, direction) ->
    if direction == 'd'
      rotate = 180
    else if direction == 'r'
      rotate = 90
    else if direction == 'l'
      rotate = 270
    else if direction == 'u'
      rotate = 0
    else
      console.log "strange direction: #{direction}"
    css_value = "rotate(#{rotate}deg)"
    $jQueryObject.css
      WebkitTransform: css_value
      MozTransform: css_value
      OTransform: css_value
      msTransform: css_value

root.Utils = Utils
