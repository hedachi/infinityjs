(function() {
  var DB, Development, GridC, NetworkClient, PieceC, Space, UIAdjuster, UIEventController, UserC, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  root.after = function(millisecond, func) {
    return setTimeout(func, millisecond);
  };

  DB = {
    id_user_map: {},
    id_piece_map: {}
  };

  root.DB = DB;

  NetworkClient = (function() {
    function NetworkClient() {}

    NetworkClient.prototype.pieces = {};

    NetworkClient.prototype.init_websocket = function() {
      var _this = this;

      if (!this.ws) {
        this.ws = new WebSocket('ws://127.0.0.1:7777/websocket');
        this.ws.onopen = function() {
          return _this.log('connection opened.');
        };
        this.ws.onclose = function(event) {
          if (event) {
            _this.log(event);
          }
          return _this.log('close.');
        };
        this.ws.onmessage = function(event) {
          var $html, data, exception, grids_data;

          try {
            if (!event.data) {
              return console.log('event has no data');
            } else {
              data = JSON.parse(event.data);
              if (data.deselect === (typeof user !== "undefined" && user !== null ? user.id : void 0)) {
                UIEventController.selecting_piece_id = null;
                $('div.piece.selected').removeClass("selected");
                $("div.grid.movable").removeClass('movable');
              }
              if (data.type === 'space') {
                grids_data = GridC.attach_prototype(data.grids);
                GridC.set_grids_data(grids_data);
                Space.show(grids_data);
                UserC.update_map(data.users);
                PieceC.update_map(data.pieces);
              } else if (data.type === 'join') {
                root.user = new UserC(data.user);
              } else {
                console.log("received a message:");
                console.log(data);
              }
              if (data.message) {
                $html = $("<div>" + data.message + "</div>");
                $html.find('.user_name').each(function(index, user_name_dom) {
                  var $user_name, color, _ref;

                  $user_name = $(user_name_dom);
                  color = (_ref = DB.id_user_map[$user_name.data('user_id')]) != null ? _ref.color : void 0;
                  return $user_name.css({
                    background: color
                  });
                });
                return $("div#news").prepend($html);
              }
            }
          } catch (_error) {
            exception = _error;
            console.log(event.data);
            throw exception;
          }
        };
        return this.ws.onerror = function(event) {
          return _this.log('ERROR!');
        };
      }
    };

    NetworkClient.prototype.send = function() {
      return this.ws.send($('#sended_value').val());
    };

    NetworkClient.prototype.connect = function() {
      return this.init_websocket();
    };

    NetworkClient.prototype.close = function() {
      this.ws.close();
      return this.ws = null;
    };

    NetworkClient.prototype.send_command = function(json) {
      return this.ws.send(JSON.stringify(json));
    };

    NetworkClient.prototype.log = function(text) {
      return console.log(text);
    };

    return NetworkClient;

  })();

  UserC = (function() {
    function UserC(user_data) {
      this.id = user_data.id;
      this.update_data(user_data);
    }

    UserC.prototype.update_data = function(user_data) {
      $('span#user_name').html(this.id);
      this.having_piece_ids = user_data.having_piece_ids;
      this.show_having_pieces();
      this.last_put_time = Date.create(user_data.last_put_time).getTime();
      $('span#user_name').css({
        background: user_data.color
      });
      if (user_data.is_joining) {
        return $("input#join_button").hide();
      } else {
        return $("input#join_button").show();
      }
    };

    UserC.prototype.update_waiting_time = function() {
      var HAVE_TO_WAIT_SECOND, now, waiting_second;

      HAVE_TO_WAIT_SECOND = Config.waiting_second;
      now = (new Date).getTime();
      waiting_second = HAVE_TO_WAIT_SECOND - Math.ceil((now - this.last_put_time) / 1000);
      if (waiting_second < 0) {
        $('div#availability').html("次の手を指せます！！！！");
        return $('div#user_status').removeClass('waiting');
      } else {
        $('div#availability').html("あと " + waiting_second + " 秒お待ち下さい...");
        return $('div#user_status').addClass('waiting');
      }
    };

    UserC.prototype.having_pieces = function() {
      var _this = this;

      return this.having_piece_ids.map(function(piece_id) {
        return DB.id_piece_map[piece_id];
      });
    };

    UserC.prototype.show_having_pieces = function() {
      var _this = this;

      if (!Utils.is_same_as_before("Client#show_having_pieces", this.having_pieces())) {
        return setTimeout(function() {
          $('div#having_pieces').empty();
          return _this.having_piece_ids.forEach(function(piece_id) {
            return $('div#having_pieces').append(PieceC.create_by_id(piece_id));
          });
        }, 500);
      }
    };

    UserC.show_all_users_information = function() {
      $("div#users").html("参加者: ");
      return Object.values(DB.id_user_map).forEach(function(user) {
        return $("div#users").append(" <span style='background-color:" + user.color + "'>" + user.id + "</span> ");
      });
    };

    UserC.update_map = function(user_datas) {
      var _this = this;

      if (!Utils.is_same_as_before("UserC#update_map_all", user_datas)) {
        user_datas.forEach(function(user_data) {
          return DB.id_user_map[user_data.id] = user_data;
        });
        this.show_all_users_information();
      }
      return this.update_current_user(user_datas);
    };

    UserC.update_current_user = function(user_datas) {
      var _this = this;

      if (user_datas) {
        return user_datas.forEach(function(user_data) {
          if (user_data.id === user.id) {
            if (!Utils.is_same_as_before("UserC#update_map_current_user", user_data)) {
              return user.update_data(user_data);
            }
          }
        });
      }
    };

    UserC.schedule_update_waiting_time = function() {
      return setInterval((function() {
        return user.update_waiting_time();
      }), 200);
    };

    return UserC;

  })();

  root.UserC = UserC;

  UIEventController = (function() {
    function UIEventController() {}

    UIEventController.selecting_piece_id = null;

    UIEventController.attach_events = function() {
      var _this = this;

      $(document).on('click', 'div.grid', function(event) {
        var $target;

        $target = $(event.target);
        console.log("grid clicked event_target_id: " + ($target.attr('id')));
        if (!$target.hasClass('grid')) {
          $target = $target.parent();
        }
        if ($target.hasClass('grid movable')) {
          console.log("$target.hasClass('grid movable')");
          network_client.send_command({
            command: 'put_piece',
            params: {
              piece_id: _this.selecting_piece_id,
              x: $target.data('x'),
              y: $target.data('y')
            }
          });
        } else {
          console.log($target);
        }
        return false;
      });
      $(document).on('click', 'div.piece', function(event) {
        var $target_piece, is_owners_piece;

        console.log('piece clicked');
        $target_piece = $(event.target);
        is_owners_piece = $target_piece.data('owner_id') === user.id;
        if ($target_piece.attr('id') === _this.selecting_piece_id) {
          $('div.piece.selected').removeClass("selected");
          PieceC.hide_movable_xys();
          _this.selecting_piece_id = null;
          return false;
        } else if (!is_owners_piece && _this.selecting_piece_id) {
          return true;
        } else if (is_owners_piece) {
          PieceC.show_movable_xys($target_piece);
          $('div.piece.selected').removeClass("selected");
          _this.selecting_piece_id = $target_piece.attr('id');
          $target_piece.addClass("selected");
          return false;
        } else {
          PieceC.show_movable_xys($target_piece);
          return true;
        }
      });
      $(document).on('click', 'div.arrow', function(event) {
        console.log('arrow clicked');
        if (_this.selecting_piece_id) {
          return network_client.send_command({
            command: 'change_direction',
            params: {
              piece_id: _this.selecting_piece_id,
              direction: $(event.target).data('direction')
            }
          });
        }
      });
      $(document).on('click', 'input#join_button', function(event) {
        return network_client.send_command({
          command: 'join'
        });
      });
      $(document).on('click', 'input#server_error_button', function(event) {
        return network_client.send("えらー");
      });
      $(document).on('click', 'input#change_name', function(event) {
        var name;

        name = prompt('名前を入力して下さい');
        if (name) {
          return network_client.send_command({
            command: 'change_name',
            params: {
              name: name
            }
          });
        }
      });
      return $(document).on('click', 'input#send_message', function(event) {
        var text;

        text = $('input#message').val();
        if (text) {
          $('input#message').val("");
          return network_client.send_command({
            command: 'chat',
            params: {
              message: text
            }
          });
        }
      });
    };

    return UIEventController;

  })();

  GridC = (function() {
    function GridC() {}

    GridC.grids_data = null;

    GridC.attach_prototype = function(grids_data) {
      return grids_data.map(function(grids_group) {
        return grids_group.map(function(grid_data) {
          var new_data, _ref;

          new_data = Object.clone(grid_data);
          new_data.__proto__ = Grid.prototype;
          if ((_ref = new_data.piece) != null) {
            _ref.__proto__ = Piece.prototype;
          }
          return new_data;
        });
      });
    };

    GridC.set_grids_data = function(grids_data) {
      return this.grids_data = grids_data;
    };

    GridC.exists = function(id) {
      return $("#" + id).size() > 0;
    };

    GridC.get_id = function(grid_data) {
      return "grid_" + grid_data.x + "_" + grid_data.y;
    };

    GridC.get_id_by_xy = function(xy) {
      return "grid_" + xy[0] + "_" + xy[1];
    };

    GridC.create = function(grid_data) {
      var $grid, id;

      id = this.get_id(grid_data);
      $grid = $("<div class='grid' id='" + id + "'data-x='" + grid_data.x + "' data-y='" + grid_data.y + "'></div>");
      $grid.css(this.get_position([grid_data.x, grid_data.y]));
      $('#board').append($grid);
      $grid.append("<div class='flag'></div>");
      return $grid;
    };

    GridC.get_position = function(xy) {
      return {
        top: "" + (xy[1] * 40) + "px",
        left: "" + (xy[0] * 40) + "px"
      };
    };

    return GridC;

  })();

  root.GridC = GridC;

  Space = (function() {
    function Space() {}

    Space.show = function(space_data) {
      var _this = this;

      return space_data.forEach(function(space_row_data, x) {
        return space_row_data.forEach(function(grid_data, y) {
          var $flag, $grid, $piece, grid_id, owner;

          grid_id = GridC.get_id(grid_data);
          if (GridC.exists(grid_id)) {
            $grid = $("#" + grid_id);
          } else {
            $grid = GridC.create(grid_data);
          }
          if (grid_data.is_wall) {
            $grid.addClass('wall');
          }
          $piece = $grid.find("div.piece");
          if (!grid_data.piece || grid_data.piece.id !== $piece.attr('id')) {
            $grid.find("div.piece").detach();
          }
          if (grid_data.piece && !Utils.is_same_as_before("Space#show_" + x + "_" + y, grid_data)) {
            $("#" + grid_data.piece.id).detach();
            $grid.append(PieceC.create(grid_data.piece));
          }
          if (grid_data.rular_id) {
            owner = function() {
              return DB.id_user_map[grid_data.rular_id];
            };
            $flag = $("#" + grid_id + " div.flag");
            if (owner()) {
              return $("#" + grid_id + " div.flag").css({
                "background-color": owner().color
              });
            } else {
              return after(200, function() {
                return $("#" + grid_id + " div.flag").css({
                  "background-color": owner().color
                });
              });
            }
          }
        });
      });
    };

    return Space;

  })();

  PieceC = (function() {
    function PieceC() {}

    PieceC.create = function(piece_data) {
      var $piece, owner;

      $piece = $("<div class='piece'>" + piece_data.type + "</div>");
      owner = function() {
        return DB.id_user_map[piece_data.owner_id];
      };
      if (owner()) {
        $piece.css({
          background: owner().color
        });
      } else {
        after(200, function() {
          return $piece.css({
            background: owner().color
          });
        });
      }
      $piece.attr('data-owner_id', piece_data.owner_id);
      Utils.rotate($piece, piece_data.direction);
      $piece.attr('id', piece_data.id);
      if (piece_data.is_promoted) {
        $piece.addClass('promoted');
      }
      return $piece;
    };

    PieceC.create_by_id = function(id) {
      var piece_data;

      piece_data = DB.id_piece_map[id];
      if (piece_data) {
        return this.create(piece_data);
      } else {
        return console.log("piece_data is nothing");
      }
    };

    PieceC.update_map = function(id_piece_map) {
      var _this = this;

      return $.each(id_piece_map, function(id, piece) {
        piece.__proto__ = Piece.prototype;
        piece.is_made_in_PieceC_update_map = true;
        return DB.id_piece_map[id] = piece;
      });
    };

    PieceC.hide_movable_xys = function() {
      return $("div.grid.movable").removeClass('movable');
    };

    PieceC.show_movable_xys = function($target_piece) {
      var $parent_grid, is_field_piece, piece, xy, xys;

      piece = DB.id_piece_map[$target_piece.attr('id')];
      $parent_grid = $target_piece.parent();
      xy = [$parent_grid.data('x'), $parent_grid.data('y')];
      console.log(xy);
      this.hide_movable_xys();
      is_field_piece = $parent_grid.hasClass("grid");
      if (is_field_piece) {
        xys = PieceMovement.get_xys_of_piece(piece, xy, GridC.grids_data);
        console.log('hoge');
      } else {
        xys = PieceMovement.get_having_pieces_movable_xys(root.user, GridC.grids_data);
        console.log(xys);
      }
      return xys.forEach(function(xy) {
        var grid_id;

        grid_id = GridC.get_id_by_xy(xy);
        return $("#" + grid_id).addClass('movable');
      });
    };

    return PieceC;

  })();

  root.PieceC = PieceC;

  Development = (function() {
    function Development() {}

    Development.create_buttons = function() {
      return $.each({}, function(action, value) {
        var $button, $div,
          _this = this;

        $div = $("<div></div>");
        $button = $("<input type='button' value='" + action + "' id='" + action + "' />");
        $button.click(function() {
          if (network_client[action]) {
            return network_client[action]();
          } else {
            return network_client.send_command({
              command: action,
              params: JSON.parse($("#" + action + "_value").val())
            });
          }
        });
        $div.append($button);
        $div.append("<input type='text' value='" + (JSON.stringify(value)) + "' id='" + action + "_value' />");
        return $('#ui_dev').append($div);
      });
    };

    return Development;

  })();

  UIAdjuster = (function() {
    function UIAdjuster() {}

    UIAdjuster.grid_height = null;

    UIAdjuster.margin_for_ui = 100;

    UIAdjuster.run = function() {
      return setTimeout(UIAdjuster.set_board_height, 200);
    };

    UIAdjuster.set_board_height = function() {
      var height;

      UIAdjuster.grid_height = $("div.grid:first").height();
      if (!UIAdjuster.grid_height) {
        return UIAdjuster.run();
      } else {
        height = GridC.grids_data[0].length * UIAdjuster.grid_height + UIAdjuster.margin_for_ui + 'px';
        console.log("set height: " + height);
        $("div#board").css('height', height);
        return setTimeout(UIAdjuster.set_board_height, 5000);
      }
    };

    return UIAdjuster;

  })();

  $(function() {
    var div, network_client;

    div = $('body').append($('<div />'));
    network_client = new NetworkClient;
    root.network_client = network_client;
    UIEventController.attach_events();
    Development.create_buttons();
    network_client.connect();
    UIAdjuster.run();
    return UserC.schedule_update_waiting_time();
  });

}).call(this);
