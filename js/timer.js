(function() {
  var Timer, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  Timer = (function() {
    function Timer() {
      this.times = [];
    }

    Timer.prototype.start = function() {
      var _ref;

      if ((_ref = root.libs) != null ? _ref.microtime : void 0) {
        this.first = root.libs.microtime.now();
      } else {
        this.first = (new Date).getTime();
      }
      return this.lap('start');
    };

    Timer.create_and_start = function() {
      var t;

      t = new Timer;
      t.start();
      return t;
    };

    Timer.prototype.lap = function(msg) {
      var _ref;

      if ((_ref = root.libs) != null ? _ref.microtime : void 0) {
        return this.times.push([msg, root.libs.microtime.now()]);
      } else {
        return this.times.push([msg, (new Date).getTime()]);
      }
    };

    Timer.prototype.end = function() {
      return this.lap('end');
    };

    Timer.prototype.show = function(use_console_log) {
      var report,
        _this = this;

      if (use_console_log == null) {
        use_console_log = true;
      }
      this.lap('timer#show called');
      if (this.times.last().last() === this.first) {
        report = "(no time)";
      } else {
        report = this.times.map(function(msg_and_time) {
          var str, _ref;

          str = "" + (msg_and_time[1] - _this.first);
          if (msg_and_time[0]) {
            if ((_ref = root.libs) != null ? _ref.microtime : void 0) {
              str += " microsec at " + msg_and_time[0];
            } else {
              str += " msec at " + msg_and_time[0];
            }
          }
          return str;
        }).join("\n");
        if (use_console_log) {
          console.log(report);
        }
      }
      return report;
    };

    return Timer;

  })();

  module.exports = Timer;

}).call(this);
