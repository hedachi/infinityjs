(function() {
  var root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  root.Config = {
    port: 7777,
    host: 'localhost',
    waiting_second: 2
  };

}).call(this);
