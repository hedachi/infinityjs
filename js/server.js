(function() {
  var GameServer, IGNORE_URI, Server, Timer, WebSocket, common_configs, fs, http, modules_sub_path, root, server_configs;

  root = typeof global !== "undefined" && global !== null ? global : window;

  fs = require('fs');

  http = require('http');

  WebSocket = require('ws');

  modules_sub_path = '../node_modules';

  require("" + modules_sub_path + "/sugar");

  Timer = require('./timer');

  GameServer = require('./game_server');

  require('./user');

  IGNORE_URI = ['favicon.ico'];

  common_configs = {
    port: 7777
  };

  server_configs = {
    host: '127.0.0.1'
  };

  Server = (function() {
    function Server() {
      this.server = null;
      this.game_server = new GameServer;
      this.wsConnections = [];
      this.free_users = [];
    }

    Server.prototype.start = function() {
      this.server = http.createServer(this.handle_request.bind(this));
      this.server.listen(common_configs.port, server_configs.host);
      return this.init_ws();
    };

    Server.prototype.schedule_update = function() {
      return setInterval(this.update.bind(this), 5000);
    };

    Server.prototype.update = function() {
      var _this = this;

      return this.game_server.get_space({}, function(json) {
        return _this.broadcast(json);
      });
    };

    Server.prototype.broadcast = function(message) {
      return this.wsConnections.forEach(function(socket) {
        if (socket.readyState === WebSocket.OPEN) {
          return socket.send(message);
        }
      });
    };

    Server.prototype.broadcast_connected_sockets = function(message) {
      return this.broadcast('[connected sockets]' + this.wsConnections.length);
    };

    Server.prototype.init_ws = function() {
      var _this = this;

      this.wsServer = new WebSocket.Server({
        server: this.server,
        path: '/websocket'
      });
      console.log("game server started listening on port:" + common_configs.port + " ...");
      return this.wsServer.on('connection', function(ws) {
        var IS_AUTO_JOINING, user;

        console.log("WebSocketServer connected");
        _this.wsConnections.push(ws);
        user = _this.free_users.pop();
        if (!user) {
          user = new root.User(_this.game_server.space);
          IS_AUTO_JOINING = false;
          if (IS_AUTO_JOINING) {
            _this.game_server.join({
              user: user
            }, function(res) {
              return console.log(res);
            });
          }
        }
        user.on_connection_opened();
        user.is_connected = true;
        ws.on('message', function(message) {
          var e, json, params;

          try {
            console.log('receive request: ' + message);
            json = JSON.parse(message);
            if (!json.params) {
              params = {
                user: user
              };
            } else {
              params = Object.merge(json.params, {
                user: user
              });
            }
            if (_this.game_server[json.command]) {
              _this.game_server[json.command](params, function(ret_string) {
                return _this.broadcast(ret_string);
              });
              return _this.update();
            }
          } catch (_error) {
            e = _error;
            console.log("############################################################");
            console.log("ERROR REPORT: " + new Date);
            console.log("------------------------------------------------------------");
            console.log(e.stack);
            return console.log("############################################################");
          }
        });
        ws.on('close', function() {
          console.log('stopping client send "close"');
          user.on_connection_closed();
          _this.free_users.push(user);
          return _this.wsConnections = _this.wsConnections.filter(function(conn, i) {
            return conn !== ws;
          });
        });
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(JSON.stringify({
            type: "join",
            user: user
          }));
          return _this.update();
        }
      });
    };

    Server.prototype.handle_request = function(req, res) {
      var error, results, timer;

      console.log("called for " + req.url);
      timer = Timer.create_and_start();
      results = {};
      try {
        this.execute(req.url, function(res_body) {
          console.log("Server#execute() finished.");
          timer.show();
          if (res) {
            res.writeHead(200, {
              'Content-Type': 'application/json'
            });
            return res.end(res_body);
          } else {
            return results = res_body;
          }
        });
      } catch (_error) {
        error = _error;
        console.log(error.stack);
        if (res) {
          res.writeHead(200, {
            'Content-Type': 'application/json'
          });
          res.end('{"result":"error"}');
        }
      }
      return results;
    };

    Server.prototype.execute = function(url, cb) {
      var command, commands, params;

      commands = url.split('?').first().split('/');
      command = commands.last();
      params = this.get_parameters(url);
      if (IGNORE_URI.find(function(url) {
        return url === command;
      })) {
        console.log("not allowd.");
        return cb("uho.");
      } else {
        return this.game_server[command](params, cb);
      }
    };

    Server.prototype.get_parameters = function(url) {
      var params, params_strings;

      params_strings = url.split('?')[1];
      params = [];
      if (params_strings != null) {
        params_strings.split('&').each(function(str) {
          return params[str.split('=')[0]] = str.split('=')[1];
        });
      }
      return params;
    };

    return Server;

  })();

  root.Server = Server;

}).call(this);
