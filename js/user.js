(function() {
  var User, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  User = (function() {
    function User(space) {
      this.id = "u_" + root.Utils.create_id();
      this.color = "rgb(" + (Number.random(0, 255)) + ", " + (Number.random(0, 255)) + ", " + (Number.random(0, 255)) + ")";
      this.is_connected = void 0;
      this.having_piece_ids = [];
      this.is_joining = false;
      space.users.push(this);
      root.DB.id_user_map[this.id] = this;
      this.last_put_time = Date.create(2000, 1, 1);
      this._name = null;
    }

    User.prototype.set_name = function(name) {
      return this._name = name;
    };

    User.prototype.name = function() {
      return (this._name || this.id) + "さん";
    };

    User.prototype.name_html_tag = function() {
      return "<span class='user_name' data-user_id='" + this.id + "'>" + (this.name()) + "</span>";
    };

    User.prototype.on_connection_closed = function() {
      return this.is_connected = false;
    };

    User.prototype.on_connection_opened = function() {
      return this.is_connected = true;
    };

    User.prototype.gain_piece = function(piece, space) {
      piece.die();
      if (piece.type === '王') {
        this.slay(piece.owner(), space);
        return piece.remove_from_the_game();
      } else {
        piece.owner_id = this.id;
        return this.having_piece_ids.push(piece.id);
      }
    };

    User.prototype.slay = function(opponent, space) {
      var IS_PIECE_REMAINING,
        _this = this;

      IS_PIECE_REMAINING = true;
      User.get_all_pieces(opponent.id).forEach(function(piece) {
        if (IS_PIECE_REMAINING) {
          return piece.owner_id = _this.id;
        } else {
          return _this.gain_piece(piece, space);
        }
      });
      space.get_territory_grids_of(opponent).forEach(function(grid) {
        return grid.rular_id = _this.id;
      });
      this.having_piece_ids.include(opponent.having_piece_ids);
      opponent.having_piece_ids = [];
      return opponent.is_joining = false;
    };

    User.get_all_pieces = function(user_id) {
      var _this = this;

      return Object.values(root.DB.id_piece_map).filter(function(piece) {
        return piece.owner_id === user_id;
      });
    };

    User.prototype.having_pieces = function() {
      return this.having_piece_ids.map(function(piece_id) {
        return root.DB.id_piece_map[piece_id];
      });
    };

    User.prototype.having_grids = function() {
      return this.having_piece_ids.map(function(piece_id) {
        return root.DB.id_piece_map[piece_id];
      });
    };

    User.prototype.is_available_time = function() {
      return ((new Date).getTime() - this.last_put_time.getTime()) / 1000 > root.Config.waiting_second;
    };

    return User;

  })();

  root.User = User;

}).call(this);
