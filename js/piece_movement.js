(function() {
  var PieceMovement, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  PieceMovement = (function() {
    function PieceMovement() {}

    PieceMovement.get_having_pieces_movable_xys = function(user, grids) {
      var grids_of_field_pieces, xys,
        _this = this;

      xys = [];
      grids_of_field_pieces = this.get_grids_of_field_pieces(user, grids);
      grids_of_field_pieces.forEach(function(grid) {
        xys.push([grid.x, grid.x]);
        return xys = xys.concat(_this.get_movable_xys_by_absolute_xy([grid.x, grid.y], grids));
      });
      xys = xys.filter(function(xy) {
        var grid, _ref;

        grid = (_ref = grids[xy[0]]) != null ? _ref[xy[1]] : void 0;
        return grid && !grid.is_wall && (grid.piece == null);
      });
      return xys.unique();
    };

    PieceMovement.get_grids_of_field_pieces = function(user, grids) {
      var grids_of_field_pieces,
        _this = this;

      return grids_of_field_pieces = root.User.get_all_pieces(user.id).filter(function(piece) {
        return !user.having_piece_ids.some(piece.id);
      }).map(function(piece) {
        return root.Grid.find_grid_by_piece_id(piece.id, grids);
      });
    };

    PieceMovement.get_movable_xys_by_absolute_xy = function(absolute_xy, grids) {
      var xys,
        _this = this;

      return xys = PieceMovement.surrounding_grids([1, 2, 3]).map(function(xy) {
        return [absolute_xy[0] + xy[0], absolute_xy[1] + xy[1]];
      }).filter(function(xy) {
        var _ref;

        return ((_ref = grids[xy[0]]) != null ? _ref[xy[1]] : void 0) != null;
      });
    };

    PieceMovement.surrounding_grids = function(ranges) {
      var range, xy, _i, _j, _len, _len1, _ref, _surrounding_xys;

      if (ranges == null) {
        ranges = [1];
      }
      _surrounding_xys = new Array;
      for (_i = 0, _len = ranges.length; _i < _len; _i++) {
        range = ranges[_i];
        _ref = this.xy_of_range(range);
        for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
          xy = _ref[_j];
          _surrounding_xys.push(xy);
        }
      }
      return _surrounding_xys;
    };

    PieceMovement.xy_of_range = function(range) {
      var results, x, x_sign, y, y_sign,
        _this = this;

      if (range == null) {
        range = 1;
      }
      results = new Array;
      x = 0;
      y = -range;
      x_sign = -1;
      y_sign = -1;
      (range * 4).times(function(num) {
        if (Math.abs(x) === range) {
          x_sign *= -1;
        }
        if (Math.abs(y) === range) {
          y_sign *= -1;
        }
        x += x_sign;
        y += y_sign;
        return results.push([x, y]);
      });
      return results;
    };

    PieceMovement.get_xys_of_piece_and_space = function(piece, space) {
      var grid, piece_current_abs_xy;

      grid = root.Grid.find_grid_by_piece_id(piece.id, space.grids);
      piece_current_abs_xy = [grid.x, grid.y];
      return this.get_xys_of_piece(piece, piece_current_abs_xy, space.grids);
    };

    PieceMovement.get_xys_of_piece = function(piece, piece_current_abs_xy, grids) {
      var abs_xys,
        _this = this;

      if (this.is_using_special_movement_logic(piece)) {
        abs_xys = [];
        this.get_movable_xys_of_type(piece.type, piece.is_promoted).forEach(function(movable_xy_group) {
          var this_group_is_available;

          this_group_is_available = true;
          return movable_xy_group.forEach(function(movable_xy_actual) {
            var grid, xy, _ref;

            if (this_group_is_available) {
              xy = _this.get_abs_xy(_this.roll(movable_xy_actual, piece.direction), piece_current_abs_xy);
              grid = (_ref = grids[xy[0]]) != null ? _ref[xy[1]] : void 0;
              if (!grid) {
                return this_group_is_available = false;
              } else {
                switch (grid.can_put(piece)) {
                  case false:
                    return this_group_is_available = false;
                  case 'move':
                    return abs_xys.push(xy);
                  case 'attack':
                    abs_xys.push(xy);
                    return this_group_is_available = false;
                }
              }
            }
          });
        });
        return abs_xys;
      } else {
        return (this.get_movable_xys_of_type(piece.type, piece.is_promoted).map(function(movable_xy) {
          return _this.get_abs_xy(_this.roll(movable_xy, piece.direction), piece_current_abs_xy);
        })).filter(function(xy) {
          var _ref, _ref1;

          return (_ref = grids[xy[0]]) != null ? (_ref1 = _ref[xy[1]]) != null ? _ref1.can_put(piece) : void 0 : void 0;
        });
      }
    };

    PieceMovement.is_using_special_movement_logic = function(piece) {
      return piece.type === '飛' || piece.type === '角' || (piece.type === '香' && !piece.is_promoted);
    };

    PieceMovement.get_movable_xys_of_type = function(type, is_promoted) {
      switch (type) {
        case '歩':
          if (!is_promoted) {
            return [[0, -1]];
          } else {
            return this.XYS_GOLD;
          }
          break;
        case '香':
          if (!is_promoted) {
            return [this.XYS_FORWARD];
          } else {
            return this.XYS_GOLD;
          }
          break;
        case '桂':
          if (!is_promoted) {
            return [[1, -2], [-1, -2]];
          } else {
            return this.XYS_GOLD;
          }
          break;
        case '飛':
          if (!is_promoted) {
            return [this.XYS_FORWARD, this.XYS_BACK, this.XYS_LEFT, this.XYS_RIGHT];
          } else {
            return [this.XYS_FORWARD, this.XYS_BACK, this.XYS_LEFT, this.XYS_RIGHT, [[-1, -1]], [[1, -1]], [[-1, 1]], [[1, 1]]];
          }
          break;
        case '角':
          if (!is_promoted) {
            return [this.XYS_FORWARD_LEFT, this.XYS_FORWARD_RIGHT, this.XYS_BACK_LEFT, this.XYS_BACK_RIGHT];
          } else {
            return [this.XYS_FORWARD_LEFT, this.XYS_FORWARD_RIGHT, this.XYS_BACK_LEFT, this.XYS_BACK_RIGHT, [[-1, 0]], [[1, 0]], [[0, 1]], [[0, -1]]];
          }
          break;
        case '金':
          return this.XYS_GOLD;
        case '銀':
          if (!is_promoted) {
            return [[0, -1], [-1, -1], [1, -1], [-1, 1], [1, 1]];
          } else {
            return this.XYS_GOLD;
          }
          break;
        case '王':
          return [[-1, -1], [0, -1], [1, -1], [1, 0], [1, 1], [0, 1], [-1, 1], [1, 1], [-1, 0]];
        default:
          return [];
      }
    };

    PieceMovement.get_abs_xy = function(relative_xy, absolute_xy) {
      return [absolute_xy[0] + relative_xy[0], absolute_xy[1] + relative_xy[1]];
    };

    PieceMovement.roll = function(relative_xy, direction) {
      var x, y;

      if (direction === 'u') {
        x = relative_xy[0];
        y = relative_xy[1];
      }
      if (direction === 'r') {
        x = -1 * relative_xy[1];
        y = relative_xy[0];
      }
      if (direction === 'l') {
        x = relative_xy[1];
        y = -1 * relative_xy[0];
      }
      if (direction === 'd') {
        x = -1 * relative_xy[0];
        y = -1 * relative_xy[1];
      }
      return [x, y];
    };

    PieceMovement.XYS_FORWARD = [[0, -1], [0, -2], [0, -3], [0, -4], [0, -5], [0, -6], [0, -7], [0, -8], [0, -9]];

    PieceMovement.XYS_LEFT = [[-1, 0], [-2, 0], [-3, 0], [-4, 0], [-5, 0], [-6, 0], [-7, 0], [-8, 0], [-9, 0]];

    PieceMovement.XYS_RIGHT = [[1, 0], [2, 0], [3, 0], [4, 0], [5, 0], [6, 0], [7, 0], [8, 0], [9, 0]];

    PieceMovement.XYS_BACK = [[0, 1], [0, 2], [0, 3], [0, 4], [0, 5], [0, 6], [0, 7], [0, 8], [0, 9]];

    PieceMovement.XYS_FORWARD_LEFT = [[-1, -1], [-2, -2], [-3, -3], [-4, -4], [-5, -5], [-6, -6], [-7, -7], [-8, -8], [-9, -9]];

    PieceMovement.XYS_FORWARD_RIGHT = [[1, -1], [2, -2], [3, -3], [4, -4], [5, -5], [6, -6], [7, -7], [8, -8], [9, -9]];

    PieceMovement.XYS_BACK_LEFT = [[-1, 1], [-2, 2], [-3, 3], [-4, 4], [-5, 5], [-6, 6], [-7, 7], [-8, 8], [-9, 9]];

    PieceMovement.XYS_BACK_RIGHT = [[1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6], [7, 7], [8, 8], [9, 9]];

    PieceMovement.XYS_GOLD = [[-1, -1], [0, -1], [1, -1], [-1, 0], [1, 0], [0, 1]];

    return PieceMovement;

  })();

  root.PieceMovement = PieceMovement;

}).call(this);
