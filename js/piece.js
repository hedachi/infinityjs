(function() {
  var Piece, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  Piece = (function() {
    function Piece(args) {
      this.id = "p_" + root.Utils.create_id();
      this.type = args.type;
      this.owner_id = args.owner.id;
      this.direction = args.direction;
      this.is_promoted = false;
      this.is_living = true;
      if (root.DB.id_piece_map[this.id]) {
        throw "already this piece is created: " + this.id;
      }
      root.DB.id_piece_map[this.id] = this;
    }

    Piece.prototype.die = function() {
      this.is_living = false;
      this.direction = 'u';
      return this.is_promoted = false;
    };

    Piece.prototype.remove_from_the_game = function() {
      return delete root.DB.id_piece_map[this.id];
    };

    Piece.prototype.promote_if_can = function(prev_grid, next_grid) {
      if ((prev_grid.rular_id && prev_grid.rular_id !== this.owner_id) || (next_grid.rular_id && next_grid.rular_id !== this.owner_id)) {
        return this.is_promoted = true;
      }
    };

    Piece.prototype.revive = function() {
      return this.is_living = true;
    };

    Piece.prototype.owner = function() {
      return root.DB.id_user_map[this.owner_id];
    };

    Piece.prototype.name = function() {
      return this.type;
    };

    Piece.prototype.toString = function() {
      return this.owner_id + this.type + this.direction;
    };

    Piece.prototype.set_direction = function(direction) {
      if (Piece.is_valid_direction(direction)) {
        if (this.is_living) {
          if (this.owner().is_available_time()) {
            this.direction = direction;
            this.owner().last_put_time = new Date;
            return true;
          } else {
            return false;
          }
        } else {
          this.direction = direction;
          return true;
        }
      } else {
        console.log('invalid direction');
        return false;
      }
    };

    Piece.is_valid_direction = function(direction) {
      return Object.keys(this.DIRECTIONS).some(direction);
    };

    Piece.DIRECTIONS = {
      u: '上',
      l: '左',
      d: '下',
      r: '右'
    };

    Piece.get_by_id = function(id) {
      return root.DB.id_piece_map[id];
    };

    return Piece;

  })();

  root.Piece = Piece;

}).call(this);
