(function() {
  var Space, l, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  l = console.log;

  Space = (function() {
    function Space(end_xy) {
      if (end_xy == null) {
        end_xy = [0, 0];
      }
      this.grids = [];
      this.start_xy = [0, 0];
      this.end_xy = end_xy;
      this.create_grids();
      this.users = [];
    }

    Space.prototype.get_grid = function(xy) {
      var _ref;

      return ((_ref = this.grids[xy[0]]) != null ? _ref[xy[1]] : void 0) || (function() {
        throw "grid (" + xy[0] + ", " + xy[1] + ") is not found...";
      })();
    };

    Space.prototype.expand = function(end_xy) {
      this.end_xy = end_xy;
      return this.create_grids();
    };

    Space.prototype.x_length = function() {
      return this.end_xy[0] - this.start_xy[0] + 1;
    };

    Space.prototype.y_length = function() {
      return this.end_xy[1] - this.start_xy[1] + 1;
    };

    Space.prototype.get_territory_grids_of = function(user) {
      return this.get_all_grids_as_array().filter(function(grid) {
        return user.id === grid.rular_id;
      });
    };

    Space.prototype.get_all_grids_as_array = function() {
      var grids;

      grids = [];
      this.grids.forEach(function(grids_col) {
        return grids_col.forEach(function(grid) {
          return grids.push(grid);
        });
      });
      return grids;
    };

    Space.prototype.get_grid_rows = function() {
      var grid_rows,
        _this = this;

      grid_rows = [];
      this.y_length().times(function(y) {
        _this.grids.forEach(function(grids_col) {
          return grids_col;
        });
        return grid_rows.push(_this.grids.map(function(grids_col) {
          return grids_col[y];
        }));
      });
      return grid_rows;
    };

    Space.prototype.show_console = function() {
      return this.get_grid_rows().forEach(function(grid_row) {
        return l(grid_row.join(""));
      });
    };

    Space.prototype.to_json = function() {
      return {
        type: 'space',
        grids: this.grids,
        users: this.users,
        pieces: root.DB.id_piece_map
      };
    };

    Space.prototype.create_grids = function() {
      var x, y, _base, _i, _ref, _ref1, _ref2, _results;

      _results = [];
      for (x = _i = _ref = this.start_xy[0], _ref1 = this.end_xy[0]; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; x = _ref <= _ref1 ? ++_i : --_i) {
        if ((_ref2 = (_base = this.grids)[x]) == null) {
          _base[x] = [];
        }
        _results.push((function() {
          var _base1, _j, _ref3, _ref4, _ref5, _results1;

          _results1 = [];
          for (y = _j = _ref3 = this.start_xy[1], _ref4 = this.end_xy[1]; _ref3 <= _ref4 ? _j <= _ref4 : _j >= _ref4; y = _ref3 <= _ref4 ? ++_j : --_j) {
            _results1.push((_ref5 = (_base1 = this.grids[x])[y]) != null ? _ref5 : _base1[y] = new root.Grid(x, y, this));
          }
          return _results1;
        }).call(this));
      }
      return _results;
    };

    Space.prototype.count_grid = function() {
      return (this.grids.map(function(grid_col) {
        return grid_col.length;
      })).sum();
    };

    Space.prototype.find_piece_by_id = function(piece_id) {
      return root.DB.id_piece_map[piece_id];
    };

    Space.prototype.create_piece = function(xy, type, owner, direction) {
      var e, piece;

      console.log("create_piece: " + type + " " + direction + " (" + xy[0] + "," + xy[1] + ")");
      if (!xy) {
        throw "argument xy is undefined";
      }
      piece = new root.Piece({
        type: type,
        owner: owner,
        direction: direction
      });
      try {
        this.get_grid(xy).put(piece);
      } catch (_error) {
        e = _error;
        console.log("ERROR: (" + xy[0] + "," + xy[1] + ")");
        throw e;
      }
      return piece;
    };

    Space.prototype.put_piece = function(piece_id, x, y) {
      var destination_grid, doing, from, piece, previous_grid, slain_piece, slain_piece_owner, user_wants_to_promote;

      if (piece_id === void 0 || x === void 0 || y === void 0) {
        console.log("invalid arguments: " + arguments);
      }
      piece = this.find_piece_by_id(piece_id);
      destination_grid = this.get_grid([x, y]);
      if (!piece.owner().is_available_time()) {
        console.log("ERROR: " + (piece.owner().id) + " is not ready to put next piece!");
        return false;
      } else if (piece.is_living && !root.PieceMovement.get_xys_of_piece_and_space(piece, this).find(function(xy) {
        return xy[0] === x && xy[1] === y;
      })) {
        console.log("ERROR: " + piece.type + " can't move to (" + x + ", " + y + ")...");
        console.log(root.PieceMovement.get_xys_of_piece_and_space(piece, this));
        return false;
      } else if (!piece.is_living && !root.PieceMovement.get_having_pieces_movable_xys(piece.owner(), this.grids)) {
        console.log("ERROR: " + piece.type + " can't put to (" + x + ", " + y + ")...");
        return false;
      } else if (destination_grid.can_put(piece)) {
        previous_grid = root.Grid.find_grid_by_piece_id(piece_id, this.grids);
        if (previous_grid) {
          previous_grid.piece = null;
        } else {
          piece.owner().having_piece_ids.remove(piece.id);
        }
        slain_piece = destination_grid.piece;
        slain_piece_owner = slain_piece != null ? slain_piece.owner() : void 0;
        destination_grid.put(piece);
        if (previous_grid) {
          user_wants_to_promote = true;
          if (user_wants_to_promote) {
            piece.promote_if_can(previous_grid, destination_grid);
          }
        }
        piece.owner().last_put_time = new Date;
        from = "";
        doing = "";
        if (!previous_grid) {
          from = "手駒から";
          doing = "配置しました。";
        } else if (slain_piece) {
          doing = "移動して、" + (slain_piece_owner.name_html_tag()) + "の" + slain_piece.type + "を奪取しました。";
        } else {
          doing = "移動しました。";
        }
        return "" + (piece.owner().name_html_tag()) + "は" + from + piece.type + "を(" + destination_grid.x + ", " + destination_grid.y + ")に" + doing;
      } else {
        false;
        return console.log("ERROR: can't put " + piece.id + " on that destination grid");
      }
    };

    Space.prototype.get_empty_area_for_horizontally_long_mode = function() {
      var found,
        _this = this;

      found = null;
      10..times(function(i) {
        var is_top, xy_from, xy_to;

        if (!found) {
          is_top = i % 2 === 0;
          xy_from = [i * 5, (is_top ? 0 : 6)];
          xy_to = [xy_from[0] + 9, xy_from[1] + 2];
          if (_this.search_piece(xy_from, xy_to).length === 0) {
            return found = {
              from: xy_from,
              direction: (is_top ? "d" : "u")
            };
          }
        }
      });
      return found || (function() {
        throw "empty area not found!";
      })();
    };

    Space.prototype.get_empty_area = function() {
      var found,
        _this = this;

      found = null;
      20..times(function(i) {
        if (!found) {
          return ((i + 1) * (i + 1) - (i * i)).times(function(ii) {
            var map, x, xy_from, xy_to, y;

            if (!found) {
              if (ii <= (i / 2) + 1) {
                x = i;
                y = ii;
              } else {
                x = i * 2 - ii;
                y = i;
              }
              map = _this.start_area_coordinate_map();
              xy_from = [map[x][0], map[y][0]];
              xy_to = [map[x][1], map[y][1]];
              if (_this.search_piece(xy_from, xy_to).length === 0) {
                return found = {
                  from: xy_from,
                  to: xy_to
                };
              }
            }
          });
        }
      });
      return found || (function() {
        throw "empty area not found!";
      })();
    };

    Space.prototype.start_area_coordinate_map = function() {
      var map;

      if (this._start_area_coordinate_map) {
        return this._start_area_coordinate_map;
      } else {
        map = {};
        10..times(function(i) {
          return map[i] = [i * 11, i * 11 + 10];
        });
        return this._start_area_coordinate_map = map;
      }
    };

    Space.prototype.search_piece = function(xy_from, xy_to) {
      var pieces, _i, _ref, _ref1, _results,
        _this = this;

      pieces = [];
      (function() {
        _results = [];
        for (var _i = _ref = xy_from[0], _ref1 = xy_to[0]; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
        return _results;
      }).apply(this).forEach(function(x) {
        var _i, _ref, _ref1, _results;

        return (function() {
          _results = [];
          for (var _i = _ref = xy_from[1], _ref1 = xy_to[1]; _ref <= _ref1 ? _i <= _ref1 : _i >= _ref1; _ref <= _ref1 ? _i++ : _i--){ _results.push(_i); }
          return _results;
        }).apply(this).forEach(function(y) {
          var piece, _ref, _ref1;

          piece = (_ref = _this.grids[x]) != null ? (_ref1 = _ref[y]) != null ? _ref1.piece : void 0 : void 0;
          if (piece) {
            return pieces.push(piece);
          }
        });
      });
      return pieces;
    };

    Space.calculate_relative_xy = function(direction, xy, xy_default, mode) {
      var x, y;

      if (mode === 'normal') {
        if (direction === 'd') {
          x = xy[0] + xy_default[0] + 5;
          y = xy[1] + xy_default[1] + 5;
        }
        if (direction === 'l') {
          x = -xy[1] + xy_default[0] + 5;
          y = -xy[0] + xy_default[1] + 5;
        }
        if (direction === 'r') {
          x = xy[1] + xy_default[0] + 5;
          y = xy[0] + xy_default[1] + 5;
        }
        if (direction === 'u') {
          x = -xy[0] + xy_default[0] + 5;
          y = -xy[1] + xy_default[1] + 5;
        }
      } else if (mode === 'horizontally') {
        if (direction === 'd') {
          x = xy[0] + xy_default[0] + 4;
          y = xy[1] + xy_default[1] + 4;
        }
        if (direction === 'u') {
          x = -xy[0] + xy_default[0] + 4;
          console.log("xy[1]:" + xy[1] + ", xy_default[1]:" + xy_default[1]);
          y = -xy[1] + xy_default[1] - 2;
        }
      }
      return [x, y];
    };

    Space.prototype.create_an_army = function(user, direction, xy_default, mode) {
      if (mode == null) {
        mode = 'normal';
      }
      if (mode === 'normal') {
        this.end_xy = [Math.max(this.end_xy[0], xy_default[0] + 10), Math.max(this.end_xy[1], xy_default[1] + 10)];
      } else if (mode === 'horizontally') {
        this.end_xy = [Math.max(this.end_xy[0], xy_default[0] + 9), 8];
      }
      this.create_grids();
      this.create_pieces_of_an_army(user, direction, xy_default, mode);
      this.create_walls_of_an_army(user, direction, xy_default, mode);
      return this.set_rular_of_an_army(user, direction, xy_default, mode);
    };

    Space.prototype.set_rular_of_an_army = function(user, direction, xy_default, mode) {
      var xys,
        _this = this;

      if (mode === 'normal') {
        xys = [];
        [-5, -4, -3, -2].forEach(function(y) {
          return [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5].forEach(function(x) {
            return xys.push([x, y]);
          });
        });
      } else if (mode === 'horizontally') {
        xys = [];
        [-4, -3, -2].forEach(function(y) {
          return [-4, -3, -2, -1, 0, 1, 2, 3, 4].forEach(function(x) {
            return xys.push([x, y]);
          });
        });
      }
      return xys.map(function(xy) {
        var absolute_xy;

        absolute_xy = Space.calculate_relative_xy(direction, xy, xy_default, mode);
        return _this.get_grid(absolute_xy).rular_id = user.id;
      });
    };

    Space.prototype.create_walls_of_an_army = function(user, direction, xy_default, mode) {
      var walls_xy,
        _this = this;

      if (mode === 'normal') {
        walls_xy = Space.WALLS_BOTTOM.concat(Space.WALLS_LEFT).concat(Space.WALLS_RIGHT);
      } else if (mode === 'horizontally') {
        if (direction === 'd') {
          walls_xy = Space.WALLS_RIGHT_FOR_HORIZONTALLY;
        } else if (direction === 'u') {
          walls_xy = Space.WALLS_RIGHT_FOR_HORIZONTALLY.concat(Space.WALLS_LEFT_FOR_HORIZONTALLY);
        }
      }
      return walls_xy.forEach(function(xy) {
        var absolute_xy, grid;

        absolute_xy = Space.calculate_relative_xy(direction, xy, xy_default, mode);
        grid = _this.get_grid(absolute_xy);
        return grid.is_wall = true;
      });
    };

    Space.prototype.create_pieces_of_an_army = function(user, direction, xy_default, mode) {
      var put,
        _this = this;

      put = function(type, xy) {
        xy = Space.calculate_relative_xy(direction, xy, xy_default, mode);
        console.log("call create_piece (" + xy[0] + ", " + xy[1] + ")");
        return _this.create_piece(xy, type, user, direction);
      };
      put('歩', [-4, -2]);
      put('歩', [-3, -2]);
      put('歩', [-2, -2]);
      put('歩', [-1, -2]);
      put('歩', [0, -2]);
      put('歩', [1, -2]);
      put('歩', [2, -2]);
      put('歩', [3, -2]);
      put('歩', [4, -2]);
      put('飛', [-3, -3]);
      put('角', [3, -3]);
      put('香', [-4, -4]);
      put('桂', [-3, -4]);
      put('銀', [-2, -4]);
      put('金', [-1, -4]);
      put('王', [0, -4]);
      put('金', [1, -4]);
      put('銀', [2, -4]);
      put('桂', [3, -4]);
      return put('香', [4, -4]);
    };

    Space.WALLS_BOTTOM = [[-4, -5], [-3, -5], [-2, -5], [-1, -5], [0, -5], [1, -5], [2, -5], [3, -5], [4, -5]];

    Space.WALLS_LEFT = [[-5, -5], [-5, -4], [-5, -3], [-5, -2]];

    Space.WALLS_RIGHT = [[5, -5], [5, -4], [5, -3], [5, -2]];

    Space.WALLS_RIGHT_FOR_HORIZONTALLY = [[5, -4], [5, -3], [5, -2]];

    Space.WALLS_LEFT_FOR_HORIZONTALLY = [[-5, -4], [-5, -3], [-5, -2]];

    return Space;

  })();

  root.Space = Space;

}).call(this);
