(function() {
  var GameServer, modules_sub_path, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  root.DB = {
    id_user_map: {},
    id_piece_map: {}
  };

  modules_sub_path = '/Users/hedachi/node_modules';

  require("" + modules_sub_path + "/sugar");

  require('./space');

  require('./piece');

  require('./grid');

  require('./utils');

  require('./config');

  require('./piece_movement');

  GameServer = (function() {
    GameServer.DIRECTIONS = ["r", "d", "u", "r"];

    GameServer.DIRECTIONS_HORIZONTALLY = ["d", "u", "d", "u", "d", "u", "d", "u", "d", "u", "d", "u", "d", "u", "d", "u", "d", "u", "d", "u"];

    function GameServer() {
      this.space = new root.Space;
    }

    GameServer.prototype.get_space = function(params, cb) {
      var e, space;

      try {
        space = JSON.stringify(this.space.to_json());
      } catch (_error) {
        e = _error;
        console.log(e);
        console.log(this.space);
        throw e;
      }
      return cb(space);
    };

    GameServer.prototype.expand_space = function(params, cb) {
      return this.space.expand(params.xy);
    };

    GameServer.prototype.join = function(params, cb) {
      var MODE, direction, from_and_direction, init_position, user;

      user = params.user;
      if (!user.is_joining) {
        MODE = 'horizontally';
        if (MODE === 'normal') {
          init_position = this.space.get_empty_area().from;
          direction = GameServer.DIRECTIONS.shift() || ["r", "u", "d", "l"].sample();
        } else if (MODE === 'horizontally') {
          from_and_direction = this.space.get_empty_area_for_horizontally_long_mode();
          init_position = from_and_direction.from;
          direction = from_and_direction.direction;
        }
        this.space.create_an_army(user, direction, init_position, MODE);
        user.is_joining = true;
      }
      return cb(JSON.stringify({
        type: 'join',
        text: "hoge"
      }));
    };

    GameServer.prototype.create_space = function(params, cb) {};

    GameServer.prototype.destroy_space = function(params, cb) {
      throw 'destroy';
    };

    GameServer.prototype.put_piece = function(params, cb) {
      var message, piece;

      piece = this.space.find_piece_by_id(params.piece_id);
      if (piece && piece.owner() === params.user) {
        message = this.space.put_piece(piece.id, params.x, params.y);
      }
      return cb(JSON.stringify({
        type: 'put_piece',
        message: message,
        deselect: params.user.id
      }));
    };

    GameServer.prototype.change_direction = function(params, cb) {
      var is_success, message, piece;

      if (params.piece_id === void 0 || !root.Piece.is_valid_direction(params.direction)) {
        console.log("ERROR: arguments invalid " + params);
      }
      piece = root.Piece.get_by_id(params.piece_id);
      is_success = piece.set_direction(params.direction);
      if (is_success) {
        message = "" + (params.user.name_html_tag()) + "は" + piece.type + "を" + Piece.DIRECTIONS[params.direction] + "に方向転換した。";
      }
      return cb(JSON.stringify({
        type: 'change_direction',
        message: message,
        deselect: params.user.id
      }));
    };

    GameServer.prototype.change_name = function(params, cb) {
      var previous_name;

      previous_name = params.user.name_html_tag();
      params.user.set_name(params.name);
      return cb(JSON.stringify({
        type: 'change_direction',
        message: "" + previous_name + "の名前は" + (params.user.name_html_tag()) + "に変更されました。"
      }));
    };

    GameServer.prototype.chat = function(params, cb) {
      return cb(JSON.stringify({
        type: 'chat',
        message: "" + (params.user.name_html_tag()) + ": " + params.message
      }));
    };

    return GameServer;

  })();

  module.exports = GameServer;

}).call(this);
