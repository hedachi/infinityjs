(function() {
  var Grid, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  Grid = (function() {
    function Grid(x, y, space) {
      this.id = "g_" + root.Utils.create_id();
      this.x = x;
      this.y = y;
      this.piece = null;
      this.is_wall = false;
      this.rular_id = null;
      if (!space) {
        throw "space is nessecary!";
      }
      this.space = space;
    }

    Grid.prototype.toJSON = function() {
      var hash;

      return hash = {
        id: this.id,
        x: this.x,
        y: this.y,
        piece: this.piece,
        is_wall: this.is_wall,
        rular_id: this.rular_id
      };
    };

    Grid.prototype.put = function(coming_piece) {
      var availability;

      availability = this.can_put(coming_piece);
      if (availability === 'move') {
        this.piece = coming_piece;
        if (!coming_piece.is_living) {
          this.piece.revive();
        }
        return true;
      } else if (availability === 'attack') {
        coming_piece.owner().gain_piece(this.piece, this.space);
        this.piece = coming_piece;
        return true;
      } else {
        console.log("" + (this.piece.name()) + " is here, so can't put " + (coming_piece.name()));
        return false;
      }
    };

    Grid.prototype.can_put = function(coming_piece) {
      if (this.is_wall) {
        return false;
      } else if (!this.piece) {
        return 'move';
      } else if (this.piece.owner() !== coming_piece.owner() && coming_piece.is_living) {
        return 'attack';
      } else {
        return false;
      }
    };

    Grid.find_grid_by_piece_id = function(piece_id, grids) {
      var found;

      found = null;
      grids.forEach(function(grids_col, x) {
        if (!found) {
          return grids_col.forEach(function(grid, y) {
            var _ref;

            if (((_ref = grid.piece) != null ? _ref.id : void 0) === piece_id) {
              return found = grid;
            }
          });
        }
      });
      return found;
    };

    return Grid;

  })();

  root.Grid = Grid;

}).call(this);
