(function() {
  var Grid, Server, Space, Utils, app_root, fs, l, path;

  l = console.log;

  fs = require('fs');

  path = require("path");

  app_root = "/Users/hedachi/InfinityJs";

  Server = require("" + app_root + "/js/server");

  Grid = require("" + app_root + "/js/grid");

  Space = require("" + app_root + "/js/utils");

  Utils = require("" + app_root + "/js/space");

  describe('Server', function() {
    return it('can instantiate', function() {
      var server;

      server = new Server;
      return expect(server.say()).toBe('hello');
    });
  });

}).call(this);
