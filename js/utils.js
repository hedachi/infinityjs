(function() {
  var Utils, root;

  root = typeof global !== "undefined" && global !== null ? global : window;

  Utils = (function() {
    function Utils() {}

    Utils._is_same_as_before_cache = {};

    Utils.create_id = function() {
      return Math.floor(Math.random() * 1000000000000);
    };

    Utils.rand = function(num) {
      return Math.floor(Math.random() * num);
    };

    Utils.percent = function(num) {
      return Math.floor(Math.random() * 10000) < num * 100;
    };

    Utils.are_same = function(obj1, obj2) {
      if (obj1 === obj2) {
        return true;
      } else if (obj1 instanceof Array && obj2 instanceof Array) {
        return obj1.join('_') === obj2.join('_');
      } else {
        return JSON.stringify(obj1) === JSON.stringify(obj2);
      }
    };

    Utils.is_same_as_before = function(key_string, data) {
      if (this._is_same_as_before_cache[key_string] === void 0) {
        this._is_same_as_before_cache[key_string] = data;
        return false;
      } else if (this.are_same(this._is_same_as_before_cache[key_string], data)) {
        return true;
      } else {
        this._is_same_as_before_cache[key_string] = data;
        return false;
      }
    };

    Utils.rotate = function($jQueryObject, direction) {
      var css_value, rotate;

      if (direction === 'd') {
        rotate = 180;
      } else if (direction === 'r') {
        rotate = 90;
      } else if (direction === 'l') {
        rotate = 270;
      } else if (direction === 'u') {
        rotate = 0;
      } else {
        console.log("strange direction: " + direction);
      }
      css_value = "rotate(" + rotate + "deg)";
      return $jQueryObject.css({
        WebkitTransform: css_value,
        MozTransform: css_value,
        OTransform: css_value,
        msTransform: css_value
      });
    };

    return Utils;

  })();

  root.Utils = Utils;

}).call(this);
